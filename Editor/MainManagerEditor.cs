﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(MainManager))]
public class MainManagerEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        MainManager myScript = (MainManager)target;
        if (GUILayout.Button("Water all Plants"))
        {
            myScript.WaterAllTiles();
        }
        if(GUILayout.Button("Activate Plants"))
        {
            myScript.ActivatePlanted();
        }
    }
}
