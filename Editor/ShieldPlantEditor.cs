﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(ShieldPlant))]

public class ShieldPlantEditor : Editor
{
    void OnSceneGUI()
    {
        ShieldPlant shieldRadius = (ShieldPlant)target;
        Handles.color = Color.red;
        Handles.DrawWireArc(shieldRadius.transform.position, shieldRadius.transform.up, shieldRadius.transform.forward, 360, shieldRadius.radius);
    }

}
