﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(BasePlant))]
public class BasePlantEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        BasePlant myScript = (BasePlant)target;
        if(GUILayout.Button("Activate Planted"))
        {
            myScript.Planted();
        }
    }

}
