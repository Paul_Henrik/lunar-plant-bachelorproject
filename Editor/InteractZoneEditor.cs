﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor (typeof(InteractObject))]

public class InteractZoneEditor : Editor {

	void OnSceneGUI()
    {
        InteractObject fow = (InteractObject)target;
        Handles.color = Color.white;
        Handles.DrawWireArc(fow.transform.position, fow.transform.up, fow.transform.forward, 360, fow.viewRadius);

        Vector3 viewAngleA = Quaternion.AngleAxis(fow.viewAngle / 2, fow.transform.up) * fow.transform.forward * fow.viewRadius;
        Vector3 viewAngleB = Quaternion.AngleAxis(-fow.viewAngle / 2, fow.transform.up) * fow.transform.forward * fow.viewRadius;

        Handles.DrawLine(fow.transform.position, fow.transform.position + viewAngleA);
        Handles.DrawLine(fow.transform.position, fow.transform.position + viewAngleB);

       // Handles.color = Color.red;
       // Handles.DrawLine(fow.transform.position, fow.transform.position + fow.transform.forward*2);
       // Handles.DrawLine(fow.transform.position, fow.transform.position + fow.transform.up*2);
    }
}
