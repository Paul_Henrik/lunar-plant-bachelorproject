﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(LightPlant))]
public class LightPlantEditor : Editor 
{
    void OnSceneGUI()
    {
        LightPlant lightRadius = (LightPlant)target;
        Handles.color = Color.red;
        Handles.DrawWireArc(lightRadius.transform.position, lightRadius.transform.up, lightRadius.transform.forward, 360, lightRadius.radius);
    }

}
