﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;

public class InteractObject : MonoBehaviour
{
    public Material radiusMaterial;
    public Camera cam;                  //cam the raycast comes from
    public bool clicked;                //if an object is clicked
    private PlayerInventory inventory;  //Get the players inventory.
    private bool planting;              //To prevent currentObject from changing while planting.
    [SerializeField]
    private int[] harvestLevel = new int[3];          //Current upgrade level on harvesting.

    //For interactable zone and editor
    [Range(0, 50)]
    public float viewRadius; //The radius of the circle that determines the interactable zone
    [Range(0, 360)]
    public float viewAngle; //The angle between the lines, determining the interactable zone
    public LayerMask interactable; //The layerMask we want the code to care about

    public List<Collider> interactableObjects = new List<Collider>(); //A list of all interactable objects in range
    public GameObject closestObject; //Holds the interactable object closest to the character, if any are in range
    [SerializeField]
    private GameObject currentObject; //The object the mouse hovers over.
    public MainManager mainManager; //Need to access the main manager
    private Animator animController;

    private HighlightObject tempHighlight;
    private HighlightObject.Type oldStatus;
    private WaterMeter waterTank;
    public Transform waterGunPos;
    public ParticleSystem waterGunSprut;

    //Audio
    public AudioSource wateringSound;
    public AudioSource plantingSound;
    public AudioSource harvestSound;

    [Range(0, 100)] public float harvestSpeed;
    private float harvestTimer;
    private float holdToHarvest;
    private bool releaseMouseButton;

    /// <summary>
    /// Get all the components needed from scratch
    /// </summary>
    void Awake()
    {
        cam = Camera.main;
        mainManager = GameObject.FindGameObjectWithTag("Manager").GetComponent<MainManager>();
        inventory = GetComponent<PlayerInventory>();
        animController = GetComponent<Animator>();
        waterTank = GetComponent<WaterMeter>();
        harvestTimer = 0;
    }

    void Start()
    {
        harvestSpeed = harvestLevel[GetComponent<PlayerUpgrades>().GetLevel(PlayerUpgrades.Type.Harvest)];
    }

    void Update()
    {
        if (!planting)   //Won't change currentObject if the player is planting. Prevents the radius from changing its object.
            FindInteractableObjects();

        if (Input.GetMouseButton(0) && !mainManager.overlayOpen)
        {
            UseTool();  //Uses current held tool
            releaseMouseButton = true;
        }

        if (Input.GetMouseButtonUp(0))
        {
            releaseMouseButton = false;
            harvestTimer = 0;
        }

        if (currentObject != null)
        {
            radiusMaterial.SetVector("_Center", currentObject.transform.position);  //The coordinates of the radius center on the ground for the shader.
            SetRadius();    //Sets the radius according to plant and plant type.
        }
        else
            HideRadius();   //Sets the radius width to 0 so its invisible.
        if (waterGunSprut.isPlaying)
        {
            waterGunSprut.transform.position = waterGunPos.position;
            //waterGunSprut.transform.rotation = Quaternion.FromToRotation(waterGunPos.position, Vector3.forward * 2);
        }
        if (currentObject != null)
        {
            float lengthToPlayer = (transform.position - currentObject.transform.position).magnitude;
            if (lengthToPlayer >= 10)
            {
                currentObject.GetComponent<HighlightObject>().SetMaterial(HighlightObject.Type.Normal);
                currentObject = null;
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Seed"))   //Picks up a seed
        {
            Seed.Type _seedtype = other.GetComponent<Seed>().seedType;
            inventory.AddToInventory(_seedtype, 1); //Adds the relevant seed to the inventory
            Destroy(other.gameObject);
        }
        else if (other.CompareTag("Collectable"))   //Picks up a collectable like Fruit
        {
            Sellables.Type _sellablesType = other.GetComponent<Sellables>().GetSellableType();
            inventory.AddToInventory(_sellablesType, 1);   //Adds collectable to the inventory.
            Destroy(other.gameObject);
        }
    }

    /// <summary>
    /// Finds all interactable objects within the interactable area of the player. This area is restricted by the 
    /// viewRadius and viewAngle variables, and is visible in the unity editor. 
    /// It then saves these interactable objects in a list that can be accessed throughout the script.
    /// </summary>
    void FindInteractableObjects()
    {
        interactableObjects.Clear();    //Clears the list

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);    //Shoots a ray to the center of the screen from the camera.
        RaycastHit[] hits = Physics.RaycastAll(ray, 15);    //Saves all the objects the ray hits in a list
        Debug.DrawRay(ray.origin, ray.direction * 30f, Color.blue); //Shows a ray in the editor window
        foreach (RaycastHit hit in hits)    //checks if the hit is a plant
        {
            if (hit.collider.CompareTag("Plant"))
            {
                currentObject = hit.collider.gameObject;    //Sets current object to a hit.
                SetRadius();    //Sets the radius of the effects of the plant
                break;
            }

        }

        #region Highlight object
        if (!planting)
        {
            if ((tempHighlight != null) && (tempHighlight.gameObject != currentObject) && currentObject != null)  //Sets the highlight of the object on change.
            {
                tempHighlight.SetMaterial(oldStatus);   //Sets the old current objects status to what it was.
                tempHighlight = currentObject.GetComponent<HighlightObject>();  //Sets th to the newly selected object.
                oldStatus = tempHighlight.GetStatus();  //Gets the status to change back to when changing the plant.
                if (oldStatus == HighlightObject.Type.Highlight)    //This is not allowed.
                    oldStatus = HighlightObject.Type.Normal;
                tempHighlight.SetMaterial(HighlightObject.Type.Highlight);  //Highlights the newly currentObject.
            }
            else if (tempHighlight == null && currentObject != null) //Highlights the objects. 
            {
                tempHighlight = currentObject.GetComponent<HighlightObject>();  //sets the currentObject.
                oldStatus = tempHighlight.GetStatus();  //Saves the old status to change back to
                tempHighlight.SetMaterial(HighlightObject.Type.Highlight);  //Highlights the currentObject.
            }
        }

        #endregion
        //closestObject = null;
        Collider[] interactableObjectsInRange = Physics.OverlapSphere(transform.position, viewRadius, interactable);    //KAYERS EXPLAIN


        foreach (Collider value in interactableObjectsInRange)
        {
            Vector3 dirToTarget = value.transform.position - transform.position;
            if (Vector3.Angle(transform.forward, dirToTarget) < viewAngle / 2)
                interactableObjects.Add(value);

            if (value.CompareTag("Seed"))   //Picks up a seed
            {
                value.GetComponent<Seed>().AttractToPlayer(transform.position);
                //Seed.Type _seedtype = value.GetComponent<Seed>().seedType;
                //inventory.AddToInventory(_seedtype, 1); //Adds the relevant seed to the inventory
                //Destroy(value.gameObject);
            }
            else if (value.CompareTag("Collectable"))   //Picks up a collectable like Fruit
            {
                value.GetComponent<Sellables>().AttractToPlayer(transform.position);
                //Sellables.Type _sellablesType = value.GetComponent<Sellables>().GetSellableType();
                //inventory.AddToInventory(_sellablesType, 1);   //Adds collectable to the inventory.
                //Destroy(value.gameObject);
            }
        }
    }

    private void UseTool()
    {
        switch (inventory.currentTool)
        {
            case PlayerInventory.Tool.Free:
                planting = true;
                GetComponent<ObjectPlacement>().SetObject(inventory.GetSelectedSeed()); //Plants a plant with current selected seed
                                                                                        //GetComponent<ObjectPlacement>().MouseClicked(); //Delay on the mouseclick to prevent accidently fast plantig.
                break;

            case PlayerInventory.Tool.Shovel:
                //ADD PLANT TYPE TO CHECK HOW LONG YOU IT WILL TAKE BEFORE YOU HARVEST A PLANT
                planting = false;
                HideRadius();   //Hides the material radius.
                Harvest();
                break;

            case PlayerInventory.Tool.Watercan:
                {
                    if (releaseMouseButton == true)
                        return;

                    animController.SetTrigger("Watering");
                    animController.SetTrigger("CancelPlanting");
                    if (interactableObjects.Count > 0 && waterTank.HasWater())  //Changed from interactableObjects != null to Count > 0. //(12.4) delete at 28.4 if there is no bugs.
                    {
                        waterGunSprut.Play();
                        wateringSound.Play();
                        waterTank.UseWater();
                        foreach (Collider col in interactableObjects)   //Go through the interactable list and and waters the plants.
                        {
                            if (col.CompareTag("Plant"))
                            {
                                col.gameObject.GetComponent<BasePlant>().WaterPlant();  //Waters the Plant.
                            }
                        }
                    }
                    HideRadius();
                    break;
                }
            default:
                break;
        }
    }

    //Adds a seed that is picked up to the inventory.
    private void PickUpSeed(Seed.Type sendIn)
    {
        inventory.AddToInventory(sendIn, 1);
    }

    /// <summary>
    /// Sets the radius for the plants area of effect
    /// </summary>
    private void SetRadius()
    {
        Seed.Type sType = currentObject.GetComponent<Seed>().GetSeedType(); //To get the right plant
        radiusMaterial.SetVector("_Center", currentObject.transform.position);
        float _radius = 0;  //If the plant does not have a radius it won't be seen.
        Color color = Color.blue;   //Waterplants natural color since local variables need to be set to something.
        bool radiusPlant = true;    //True if the selected plant have a radius.
        switch (sType)  //Sets the type of the radius
        {
            case Seed.Type.Shield:
                radiusMaterial.SetFloat("_RadiusWidth", 0.2f);
                _radius = currentObject.GetComponent<ShieldPlant>().radius; //Gets the plants radius.
                color = Color.magenta;
                break;
            case Seed.Type.Water:
                radiusMaterial.SetFloat("_RadiusWidth", 0.2f);
                _radius = currentObject.GetComponent<WaterPlant>().radius;
                break;
            case Seed.Type.Light:
                radiusMaterial.SetFloat("_RadiusWidth", 0.2f);
                _radius = currentObject.GetComponent<LightPlant>().radius;
                color = Color.yellow;
                break;
            default:
                radiusMaterial.SetFloat("_RadiusWidth", 0);
                radiusPlant = false;
                break;
        }
        if (radiusPlant)
        {
            radiusMaterial.SetColor("_RadiusColor", color);
            radiusMaterial.SetFloat("_CenterRadius", _radius);
        }
    }

    private void Harvest()
    {
        if (currentObject == null)
            return;
        Seed.Type seedType = currentObject.GetComponent<Seed>().seedType;

        if (harvestTimer < GetHarvestTime(seedType))
        {
            harvestTimer += Time.deltaTime * harvestSpeed;
            return;
        }

        if (currentObject.CompareTag("Plant"))
        {
            BasePlant SelecetedPlant = currentObject.GetComponent<BasePlant>();     //Makes code prettier

            SelecetedPlant.HarvestPlant(1); //Plant will be harvested, then drop loot based on wether it was fully grown

            animController.SetTrigger("Harvesting");
            harvestSound.Play();
            tempHighlight = null;   //Removes gameobject to prevent bugs
            currentObject = null;   // Ditto
            harvestTimer = 0;
        }
    }

    //Hides the circle
    private void HideRadius()
    {
        radiusMaterial.SetFloat("_RadiusWidth", 0);
    }

    //Sets the planting mode from a differente script : ObjectPlacement
    public void SetPlanting(bool set)
    {
        planting = set;
    }

    //Sets current object from another script : ObjectPlacement
    public void SetCurrentObject(GameObject setObject)
    {
        currentObject = setObject;
    }

    public float GetHarvestTime(Seed.Type seedIn)
    {
        switch (seedIn)
        {
            case Seed.Type.Normal:
                return holdToHarvest = 50;
            default:
                return holdToHarvest = 100;
        }
    }

    public float GetHarvestTimer()
    {
        return harvestTimer;
    }

    private bool HarvestTimer()
    {
        if (harvestTimer < holdToHarvest)
        {
            harvestTimer = 0;
            return true;
        }
        harvestTimer += Time.deltaTime * 10;
        return false;
    }

    public GameObject GetCurrentObject()
    {
        return currentObject;
    }


    public void SetHarvestLevel(int setLevel)
    {
        if (setLevel >= 3)
        {
            Debug.LogWarning("Value higher than array size" + gameObject.name, gameObject);
            return;
        }

        harvestSpeed = harvestLevel[setLevel];
    }
}
