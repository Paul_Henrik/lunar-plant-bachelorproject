﻿using UnityEngine;
using UnityEngine.Assertions;

public class PlayerMovement : MonoBehaviour
{
    public float speed; // The speed at which the player will move
    private float normalSpeed; //The set startspeed.
    Rigidbody rb; // Use addforce on rigidbody when jumping
    Collider asteroid; // Need to know if the player is colliding with the asteroid, so they can't jump mid-air
    public bool colliding; // used to see if the character is colliding with the asteroid. Cannot jump if they're already in the air
    Camera mainCam; // Use camera to walk in regards to where the camera is pointing (The forward vector of the camera)
    public Animator animationController; //Controller for Dae's animations

    public Vector3 moveAmount; // The direction and distance the player will move in
    Vector3 smoothVelocity; //Needed for the SmoothDamp function
    Vector3 moveDir; // The direction the player will move in
    public GameObject followpoint; // Point for the camera to follow, to not mess up the rotation
    public ParticleSystem walkDustParticles; //Dust particles behind Dae when he walks
    [Range(0,2)]public float jumpTimer;
    private float jumpTimerReset;   //Resets to jumpTimer;

    //Audiomanager and sound effects
    public GameObject AudioManager;
    public AudioSource walkingSound;
    public AudioSource jumpingUpSound;
    public AudioSource jumpingLandingSound;

    private Ray[] ray;  //Ray for checking the wall
    private Vector3 rayTest;    //Debugging the raycast
    [SerializeField]
    [Range(0, 359)]
    private float rayDegree; //Degree between ray and ground
    private int frames; //prevent raycast every frame.
   [SerializeField] private float angleSensitivity; //How step the player can walk up.


    void Awake()
    {
        //Sets variables
        speed = 8f;
        normalSpeed = speed;
        rb = GetComponent<Rigidbody>();
        asteroid = GameObject.FindGameObjectWithTag("Asteroid").GetComponent<MeshCollider>();
        colliding = false;
        mainCam = Camera.main;
        animationController = GetComponent<Animator>();
        followpoint.transform.position = transform.position;
        Assert.AreNotEqual(0, angleSensitivity, "AngleSensitivity not set on playermovement: " + gameObject.name);
        Assert.AreNotEqual(0, jumpTimer, "JumpTimer not set on playermovement: " + gameObject.name);
        jumpTimerReset = jumpTimer;
    }

    /// <summary>
    /// Rotate the player according to the mouse input. Will only rotate around the players local y-axis
    /// Recieve input for movement and use that input to calculate the moveAmount vector
    /// </summary>
    void Update()
    {
        moveDir = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical")).normalized; //The direction i want the player to move
        moveDir = mainCam.transform.rotation * moveDir; // Rotate the direction with the camera's rotation (W is away from camera)
        moveDir = Vector3.ProjectOnPlane(moveDir, transform.up); // "Straightening" out the vector for the player

        if (Time.timeScale != 0)
            transform.rotation = Quaternion.FromToRotation(transform.forward, moveDir) * transform.rotation;

        if (moveDir != Vector3.zero && !animationController.GetBool("Walking") && Time.timeScale != 0)
        {
            animationController.SetBool("Walking", true);

            if (colliding && !walkingSound.isPlaying)
                walkingSound.Play();

            walkDustParticles.Play();
        }
        else if (!colliding)
        {
            animationController.SetBool("Walking", false);
            walkingSound.mute = true;
            walkDustParticles.Stop();
        }
        else if (moveDir == Vector3.zero)
        {
            animationController.SetBool("Walking", false);
            walkingSound.Stop();
            walkDustParticles.Stop();
        }

        AudioManager.transform.position = transform.position;

        Vector3 targetMoveAmount = moveDir * speed;
        moveAmount = Vector3.SmoothDamp(moveAmount, targetMoveAmount, ref smoothVelocity, 0.2f);

        RaycastGroundAngle();   //Sends a ray to the ground and check the angle compared to the player.

        jumpTimer -= Time.deltaTime;
    }
    
    /// <summary>
    /// Calculate the jumping of the player. Use the moveAmount vector calculated in the update
    /// function to move the player by the rigidbody
    /// </summary>
    void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.Space) && colliding && jumpTimer < 0)
        {
            if (!animationController.GetBool("Jumping"))
                animationController.SetBool("Jumping", true);

            jumpingUpSound.Play();

            Vector3 jumpforce = Vector3.zero; //Create a vector that applies force in the y-axis direction
            jumpforce.y = 300;
            jumpforce = transform.up * jumpforce.magnitude; //Allign the jump direction with the player direction
            rb.AddForce(jumpforce);
            colliding = false;
            jumpTimer = jumpTimerReset;

            //RaycastHit[] rayHit = Physics.RaycastAll(transform.position, Vector3.forward, 10f);
        }

        transform.position += (moveAmount * Time.fixedDeltaTime);
        followpoint.transform.position = transform.position;
    }

    /// <summary>
    /// Prohibits the player from jumping in the air. They have to be colliding with the asteroid in order to jump.
    /// </summary>
    /// <param name="other">
    /// The collision with the asteroid. other.collider is the collider of the asteroid
    /// </param>
    void OnCollisionEnter(Collision other)
    {
        if (other.collider == asteroid || LayerMask.LayerToName(other.collider.gameObject.layer) == "Ground")
        {
            if (!colliding)
                jumpingLandingSound.Play();
            colliding = true;
            walkingSound.mute = false;
            animationController.SetBool("Jumping", false);
        }
    }

    /// <summary>
    /// Sends ray to the ground and compares the angle.
    /// </summary>
    public void RaycastGroundAngle()
    {
        frames++;
        if (frames % 10 != 0)
        {
            return;
        }
        Ray ray = new Ray();    //Creates a new ray
        Vector3 direction = Quaternion.AngleAxis(rayDegree, transform.right) * -transform.up;   //Getting the direction of the ray
        Vector3 RayStartPos = transform.up * 2 + transform.forward * 0.2f;  //Because vectors is according to world space.
        ray.direction = direction;  //Setting the direction to the ray
        ray.origin = transform.position + RayStartPos;  //Setting the startpoint of the ray
        Debug.DrawRay(ray.origin, ray.direction * 3, Color.green); //shows where the ray hits.
        RaycastHit[] hits = Physics.RaycastAll(ray, 3);
        float angle;
        foreach (var hit in hits)   //Sets the speed based on the angle of the ground
        {
            if (hit.collider.name == "ShieldPlatform" || LayerMask.LayerToName(hit.collider.gameObject.layer) == "Ground")     //So 
            {
                if(jumpTimer < 0)
                    colliding = true;
            }
            if (hit.collider.CompareTag("Asteroid"))
            {
                angle = Vector3.Angle(hit.normal, ray.origin);
                if (angle > angleSensitivity)
                    speed = speed / angle;
                else
                    speed = normalSpeed;

                // Debug.Log(Vector3.Angle(hit.normal, ray.origin));
                break;
            }
            else
                speed = normalSpeed;
            continue;
        }
    }

    public void SetAngle(float degree)
    {
        angleSensitivity = degree;
    }

    public float GetAngle()
    {
        return angleSensitivity;
    }
}
