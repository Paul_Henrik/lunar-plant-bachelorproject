﻿using UnityEngine;

public class PlayerUpgrades : MonoBehaviour
{
    public bool[] allUpgrade = new bool[8];   //For save and load. 0-2 harvest, 3-5 water, 6 bow head, 7 bow neck
    private bool[] waterUpgrade = new bool[3];  //How many unlocked levels of waterupgrade in the store.
    private int waterLevel = 0;       //Unlocked levels in ints.
    private bool[] harvestUpgrade = new bool[3];  //How many unlocked levels of harvestUpgrades in the store.
    private int harvestLevel = 0;     //Unlocked levels in harvesting.
    public bool bowtieHead = false; //Visual upgrade
    public bool bowtieNeck = false; //Visual upgrade

    public enum Type
    {
        Water,
        Harvest,
        Bowtie
    }

    public void AddLevel(Type input)
    {
        switch (input)
        {
            case Type.Water:
                {
                    if (waterLevel < waterUpgrade.Length)
                    {
                        waterUpgrade[waterLevel] = true;
                        waterLevel++;
                    }

                    GetComponent<WaterMeter>().SetWaterUpgrade(waterLevel); //sets the new level
                    break;
                }
            case Type.Harvest:
                {
                    if (harvestLevel < harvestUpgrade.Length)
                    {
                        harvestUpgrade[harvestLevel] = true;
                        harvestLevel++;
                    }

                    GetComponent<InteractObject>().SetHarvestLevel(harvestLevel);
                    break;
                }
            case Type.Bowtie:
                if(allUpgrade[6])
                {
                    bowtieHead = true;
                }
                else if (allUpgrade[7])
                {
                    bowtieNeck = true;
                }
                break;
            default:
                break;
        }
    }

    public int GetLevel(Type type)
    {
        switch (type)
        {
            case Type.Water:
                return waterLevel;
            case Type.Harvest:
                return harvestLevel;
            default:
                return 0;
        }
    }

    //spot 0-2 is harvest, 2-5 is water
    public void LoadLevel(bool[] upgradeData)
    {
        for (int i = 0; i < upgradeData.Length; i++)
        {
            allUpgrade[i] = upgradeData[i];
        }

        for (int i = 0; i < allUpgrade.Length; i++)
        {
            if (i < 3 && allUpgrade[i] == true) //mindre enn 3.
                harvestLevel++;
            if ((i >= 3 && i < 6) && allUpgrade[i] == true) //mellom 3 til 6
                waterLevel++;
        }

        for (int i = 0; i < harvestLevel; i++)
        {
            harvestUpgrade[i] = true;
        }

        for (int i = 0; i < waterLevel; i++)
        {
            waterUpgrade[i] = true;
        }

        bowtieHead = allUpgrade[6];
        bowtieNeck = allUpgrade[7];
    }


}
