﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class PlayerInventory : MonoBehaviour
{

    public Dictionary<Seed.Type, int> inventorySeed = new Dictionary<Seed.Type, int>(); //Inventory for seeds
    public Dictionary<Sellables.Type, int> inventorySellables = new Dictionary<Sellables.Type, int>();    //Inventory for sellable objects.
    public Tool currentTool; //The tool currently equipped. Is free if no tool is equipped
    private Seed.Type seedType; //Used for the seed inventory.
    [SerializeField]
    private Seed.Type selectedSeed; //Used in other scripts <Placement, InteractObject>
    private int amount; //used to add the amount to the inventory. Double check later if it is needed
    public WaterMeter waterMeter; //Reference to the water meter to set and get water level
    
    public MainManager mainManager; //For calling the freeze and unfreeze cam functions
    private Animator animController; // The animation controller for the player animations. Used to trigger certain animations
    public InventoryData data; //Holds the data for saving
    private Light waterLight; //The light that indicatesthe area that is watered

    public int renorks = 200; //How much money the player has
    public GameObject centerOfScreen; //A dot that shows up in the center of the screen when harvesting

    public GameObject[] bowties = new GameObject[2]; //The bowties that can be activated through marketplace. Visual upgrades

    public enum Tool //Enum for choosing tool. Free is no tool, shovel is harvest and watercan is watering device
    {
        Free,
        Shovel,
        Watercan
    }

    void Awake()
    {
        data = new InventoryData();
        mainManager = GameObject.FindGameObjectWithTag("Manager").GetComponent<MainManager>();
        animController = GetComponent<Animator>();
        waterLight = GetComponentInChildren<Light>();
        waterMeter = GetComponent<WaterMeter>();

        AddToInventory(Seed.Type.Normal, 5);
        AddToInventory(Seed.Type.Shield, 1);
    }

    void Start()
    {
        Assert.IsNotNull(centerOfScreen, "Add centerOfScreen from OverlayCanvas to " + gameObject.name);
        
        //AddToInventory(Seed.Type.Normal, 10);
        //AddToInventory(Seed.Type.Shield, 2);
        mainManager.discoveredPlants[1] = true; //Because shield plant has the equivalent value of 1
    }

    void Update()
    {

        switch (currentTool) //Tests for current tool, then sets variables for that tool to work
        {
            case Tool.Free:
                centerOfScreen.SetActive(false);
                GetComponent<InteractObject>().SetPlanting(true);
                Cursor.lockState = CursorLockMode.None;
                Cursor.lockState = CursorLockMode.Confined;
                break;
            case Tool.Shovel:
                centerOfScreen.SetActive(true);
                GetComponent<InteractObject>().SetPlanting(false);
                Cursor.lockState = CursorLockMode.Locked;
                break;
            case Tool.Watercan:
                centerOfScreen.SetActive(false);
                GetComponent<InteractObject>().SetPlanting(false);
                Cursor.lockState = CursorLockMode.None;
                Cursor.lockState = CursorLockMode.Confined;
                break;
            default:
                break;
        }

        if (GetComponent<PlayerUpgrades>().bowtieHead) //Activates bowties if they are bought
        {
            bowties[0].SetActive(true);
        }
        if (GetComponent<PlayerUpgrades>().bowtieNeck)
        {
            bowties[1].SetActive(true);
        }

        waterLight.enabled = (currentTool == Tool.Watercan) ? true : false; //Sets flashlight true if watering can is equipped 
        
    }

    #region SeedInventory
    /// <summary>
    /// Send object into inventory list.
    ///     <para>
    ///     Takes Sellables and Seed.Type
    ///     </para>
    /// </summary>
    /// <param name="seedIn"></param>
    /// <param name="amount"></param>
    public void AddToInventory(Seed.Type seedIn, int _amount)
    {
        seedType = seedIn;  //sets the seed that should be added to the inventory.
        amount = _amount;
        int oldTotal = 0;   //Resets the total.
        if (inventorySeed.TryGetValue(seedType, out oldTotal))  //check if the Dictionary have the seedtype, then sends out the amount of the seed to oldTotal.
            inventorySeed[seedType] = oldTotal + amount;    //Adds the amount of selected seeds to the total amount of seeds. 
        else
            inventorySeed.Add(seedType, amount); //Adds the seedtype to the dictionary and amount of seeds. 

        if(!mainManager.discoveredPlants[(int)seedIn])
        {
            mainManager.discoveredPlants[(int)seedIn] = true;
        }
    }

    /// <summary>
    /// Removes seeds from the inventory.
    /// </summary>
    /// <param name="seedIn"></param>
    /// <param name="_amount"></param>
    public void RemoveFromInventory(Seed.Type seedIn, int _amount)
    {
        seedType = seedIn;
        amount = _amount;

        int oldTotal = 0;

        if (inventorySeed.TryGetValue(seedType, out oldTotal))
        {
            inventorySeed[seedType] = oldTotal - amount;
            if(oldTotal-amount == 0)
            {
                inventorySeed.Remove(seedType);
            }
        }
        else
            return;
    }



    /// <summary>
    /// Returns a bool if the inventory has the item, also removes one seed.
    /// </summary>
    /// <param name="type"></param>
    /// <returns></returns>
    public bool useSeed(Seed.Type type)
    {
        if (currentTool == Tool.Free)
        {
            inventorySeed.TryGetValue(type, out amount);    //gets the amount of the wished seed to plant
            bool returnAnswer = (amount > 0) ? true : false;    //checks if the player has the seed.

            return returnAnswer;        //Returns the answer
        }
        else
            return false;
    }

    public Seed.Type GetSelectedSeed()
    {
        return selectedSeed;
    }

    public void SetSelectedSeed(Seed.Type set)
    {
        selectedSeed = set;
    }

    #endregion

    #region ObjectInventory

    /// <summary>
    /// Send object into inventory list.
    ///     <para>
    ///     Takes Sellables and Seed.Type
    ///     </para>
    /// </summary>
    /// <param name="itemIn"></param>
    /// <param name="_amount"></param>
    public void AddToInventory(Sellables.Type tempType, int _amount)
    {
        int oldTotal = 0;   //Resets the total.
        if (inventorySellables.TryGetValue(tempType, out oldTotal))  //check if the Dictionary have the seedtype, then sends out the amount of the seed to oldTotal.
            inventorySellables[tempType] = oldTotal + _amount;    //Adds the amount of selected seeds to the total amount of seeds. 
        else
            inventorySellables.Add(tempType, _amount); //Adds the seedtype to the dictionary and amount of seeds. 
    }

    /// <summary>
    /// Removes Object from inventory.
    /// </summary>
    /// <param name="itemIn"></param>
    /// <param name="_amount"></param>
    public void RemoveFromInventory(Sellables.Type tempType, int _amount)
    {
        int oldTotal = 0;

        if (inventorySellables.TryGetValue(tempType, out oldTotal))
        {
            inventorySellables[tempType] = oldTotal - _amount;
            if (oldTotal - _amount == 0)
            {
                inventorySellables.Remove(tempType);
            }
        }
        else
            return;
    }
    #endregion

    #region ToolInventory
    /// <summary>
    /// Sets tool, then activates animation if needed
    /// </summary>
    /// <param name="tool"></param>
    public void SetTool(Tool tool)
    {
        currentTool = tool;

        if(currentTool == Tool.Watercan)
        {
            animController.SetBool("WaterEquipped", true);
        }
        else
        {
            animController.SetBool("WaterEquipped", false);
        }
    }
  
    #endregion

    #region Debug
    /// <summary>
    /// Debugging the inventory to the console..
    /// </summary>
    /// <param name="inventory"></param>
    public void OnInventoryChange(Dictionary<string, Sellables> inventory)
    {
        foreach (var item in inventory)
        {
            Debug.Log(item.Key + " " + item.Value.GetAmount() + " Buy Price: " + item.Value.GetPrice(Sellables.Shop.Buy));
        }
    }

    public void OnInventoryChange(Dictionary<Seed.Type, int> inventory)
    {
        foreach (var item in inventory)
        {
            Debug.Log(item.Key.ToString() + " " + item.Value);
        }
    }
    #endregion

    #region SaveLoad

    void OnEnable()
    {
        SaveLoadManager.OnLoaded += LoadData;
        SaveLoadManager.OnBeforeSave += StoreData;
        SaveLoadManager.OnBeforeSave += ApplyData;
    }

    void OnDisable()
    {
        SaveLoadManager.OnLoaded -= LoadData;
        SaveLoadManager.OnBeforeSave -= StoreData;
        SaveLoadManager.OnBeforeSave -= ApplyData;
    }

    public void StoreData() //Stores the data in the data variable
    {
        data._waterLevel = waterMeter.newWaterLevel;
        data._currency = renorks;

        int tempCount = 0;

        foreach(KeyValuePair<Seed.Type, int> seed in inventorySeed)
        {
            data._seedTypes[tempCount] = (int)seed.Key;
            data._seedAmount[tempCount] = seed.Value;

            tempCount++;
        }

        tempCount = 0;

        foreach (KeyValuePair<Sellables.Type, int> sellable in inventorySellables)
        {
            data._sellableTypes[tempCount] = (int)sellable.Key;
            data._sellableAmount[tempCount] = sellable.Value;

            tempCount++;
        }
    }

    public void LoadData() //Loads the data from the data variable
    {
        waterMeter.newWaterLevel = data._waterLevel;
        renorks = data._currency;

        int tempCount = 0;

        foreach(int type in data._seedTypes)
        {
            AddToInventory(Seed.Type.Normal + type, data._seedAmount[tempCount]);

            tempCount++;
        }

        tempCount = 0;

        foreach (int type in data._sellableTypes)
        {
            AddToInventory(Sellables.Type.Fruit + type, data._sellableAmount[tempCount]);

            tempCount++;
        }

    }

    public void ApplyData()
    {
        SaveLoadManager.AddData(data);
    }

    #endregion

}

[Serializable]
public class InventoryData
{
    public float _waterLevel;
    public int _currency;
    public int[] _seedTypes = new int[6];
    public int[] _seedAmount = new int[6];
    public int[] _sellableTypes = new int[10];
    public int[] _sellableAmount = new int[10];
}