﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class ObjectPlacement : MonoBehaviour
{
	//public GameObject[] objects;
	public GameObject[] plants; //different plants that can be planted. 
	private PlaceableObject placeableObject;    //Check if other objects are in the way.
	public Transform asteroid;
	private Transform currentObject;    //The object the player is interacting with.
	private PlayerInventory inventory;  //To get seeds and what tool is being used
	private HighlightObject hi; //Change which object that is highlighted
	public float clickTimer = 0.05f; //Prevents the player to click to fast and from instantly planting
	private float clickTimerReset;  //Keeps the value of the original clicktimer
	private bool hasPlaced; //prevents double planting
	public Animator animController;
	private bool clickReady;    //Prevents instantly planting

	private void Start()
	{
		asteroid = GameObject.FindGameObjectWithTag("Asteroid").transform;
		clickTimerReset = clickTimer;
		inventory = GetComponent<PlayerInventory>();
		animController = GetComponent<Animator>();
	}

	void Update()
	{
		if (!clickReady)   //Prevents the player for planting by accident if the click to fast.
		{
			clickTimer -= Time.deltaTime;
			clickReady = (clickTimer < 0) ? true : false;
		}

		if (currentObject == null)
			return;
		
        if (!hasPlaced)    //True if a object is currently being placed
		{
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);    //Coordinates to where the object will be placed.
			GetComponent<InteractObject>().SetCurrentObject(currentObject.gameObject);  //Sets currentObject in the InteractObject script to show plants area of effect.
			currentObject.transform.rotation = transform.rotation;  //Can rotate the plant with the players rotation. Quickfix


			if ((inventory.currentTool != PlayerInventory.Tool.Free) || Input.GetMouseButtonDown(1))    //Cancels the placement
			{
				animController.SetTrigger("CancelPlanting");
				GetComponent<InteractObject>().SetPlanting(false);  //Makes interactive object script able to change currentObject.
				hi.SetMaterial(HighlightObject.Type.Normal);    //Object set to normal material.
				Destroy(currentObject.gameObject);
				GetComponent<InteractObject>().SetCurrentObject(null);
				Cursor.visible = false;
				hasPlaced = true;
				currentObject = null;
			}
			else if ((inventory.GetSelectedSeed() != currentObject.GetComponent<Seed>().seedType) || Input.GetMouseButtonDown(1))   //Cancels the placement when changing a seed.
			{
				animController.SetTrigger("CancelPlanting");
				GetComponent<InteractObject>().SetPlanting(false);  //Makes interactive object script able to change currentObject.
				hi.SetMaterial(HighlightObject.Type.Normal);    //Object set to normal material.
				Destroy(currentObject.gameObject);
				Cursor.visible = false;
				GetComponent<InteractObject>().SetCurrentObject(null);
				currentObject = null;
				hasPlaced = true;
			}

			Debug.DrawRay(ray.origin, ray.direction * 20, Color.green); //shows the ray in editor window.

			if (IsLegalPosition())  //If there is no obstacles in the way, the plant is green and can be planted.
				hi.SetMaterial(HighlightObject.Type.Highlight);
			else
				hi.SetMaterial(HighlightObject.Type.NotAvailable);  //There is a obstacle in the way and the plant cannot be planted. 
			RaycastHit[] hits = Physics.RaycastAll(ray, 20);    //Collects all the object the ray hits in a list.

            for (int i = hits.Length; i--> 0;)  //Descending forloop to get the closest target in the ray //Multiple ground objects.
            {
                if (LayerMask.LayerToName(hits[i].collider.gameObject.layer) == "Ground")
                {
                    if (currentObject != null)
                        currentObject.transform.position = hits[i].point;
                }

                if (Input.GetKeyDown(KeyCode.E) || Input.GetMouseButtonDown(0) && clickReady) //plant with a click. Also prevents instaplant
                {
                    if (IsLegalPosition())
                    {
                        animController.SetTrigger("PlantingPlant");
                        currentObject.GetComponent<BasePlant>().Planted();  //Starts the plants growth
                        currentObject.gameObject.layer = LayerMask.NameToLayer("Interactable"); //changes the layer from IgnoreRaycast to Interactable.
                        FindObjectOfType<MainManager>().GetComponent<Grass>().AddToPlantList(currentObject);    //Adds plant to grass shaders list.
                        inventory.RemoveFromInventory(inventory.GetSelectedSeed(), 1);
                        hi.SetMaterial(HighlightObject.Type.Normal);
                        hasPlaced = true;
                        hi.SetHologram(false);
                        hi = null;
                        currentObject = null;
                        Cursor.visible = false;
                        MouseClicked();
                        break;
                    }
                }
            }

            //If it bugs up, use this instead of forloop. (20.05.2018)
			//foreach (RaycastHit hit in hits)
			//{
			//	if (LayerMask.LayerToName(hit.collider.gameObject.layer) == "Ground")
			//	{
			//		if (currentObject != null)
			//			currentObject.transform.position = hit.point;
			//	}

			//	if (Input.GetKeyDown(KeyCode.E) || Input.GetMouseButtonDown(0) && clickReady) //plant with a click. Also prevents instaplant
			//	{
			//		if (IsLegalPosition())
			//		{
			//			animController.SetTrigger("PlantingPlant");
			//			currentObject.GetComponent<BasePlant>().Planted();  //Starts the plants growth
			//			currentObject.gameObject.layer = LayerMask.NameToLayer("Interactable"); //changes the layer from IgnoreRaycast to Interactable.
			//			FindObjectOfType<MainManager>().GetComponent<Grass>().AddToPlantList(currentObject);    //Adds plant to grass shaders list.
			//			inventory.RemoveFromInventory(inventory.GetSelectedSeed(), 1);
			//			hi.SetMaterial(HighlightObject.Type.Normal);
			//			hasPlaced = true;
			//			hi.SetHologram(false);
			//			hi = null;
			//			currentObject = null;
			//			Cursor.visible = false;
			//			MouseClicked();
			//			break;
			//		}
			//	}
			//}

		}
	}

	//Checks if there is something in the way
	private bool IsLegalPosition()
	{
		foreach (var item in placeableObject.colliders)
		{
			if (item.tag == "Plant" || item.tag == "Unplantable")
				return false;
		}

		return true;
	}

	/// <summary>
	/// Sets an object in front of the player
	/// </summary>
	public void UseObject(GameObject _object)
	{
		if (currentObject == null)
		{
			if (inventory.useSeed(_object.GetComponent<Seed>().seedType))    //Won't make the object unless the it has the used seed in the inventory
			{
				hasPlaced = false;                                              //checks that the object hasen't been placed yet
				Cursor.visible = true;
				animController.SetTrigger("PlacingPlant");
                GetComponent<InteractObject>().plantingSound.Play();
				currentObject = (Instantiate(_object)).transform;       //The object that is about to be placed
				placeableObject = currentObject.GetComponent<PlaceableObject>();    //To check if the object is colliding with other objects.
				hi = currentObject.GetComponent<HighlightObject>();                 //Makes the code abit tidier. TIDYBOYS ONTZ ONTZ ONTZ
				hi.SetHologram(true);                                               //Makes the object turn into a hologram.
				MouseClicked();
			}
		}
	}

	/// <summary>
	/// Starts a timer which prevents an object to be set after a mouseclick.
	/// </summary>
	public void MouseClicked()
	{
		clickTimer = clickTimerReset;
		clickReady = false;
	}



	public void SetObject(Seed.Type type)
	{
		switch (type)
		{
			case Seed.Type.Normal:
				UseObject(plants[0]);
				break;
			case Seed.Type.Shield:
				UseObject(plants[1]);
				break;
			case Seed.Type.Water:
				UseObject(plants[2]);
				break;
			case Seed.Type.Light:
				UseObject(plants[3]);
				break;
			case Seed.Type.Fruit:
				UseObject(plants[4]);
				break;
            case Seed.Type.Crocus:
                UseObject(plants[5]);
                break;
			default:
				break;
		}

	}
}
