﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;

public class WaterMeter : MonoBehaviour
{
    public Image waterMeterLeft;
    public Image waterMeterRight;
    public Image[] counter = new Image[3];  //The percentage
    public Sprite[] numbers = new Sprite[10];   //The numbers used in the counter
    [SerializeField]
    private float fillSpeed;    //How fast the watertank fills up
    private float fillSpeedDisplay = 100;   //How fast the watertanks display fills up, smooth transsison 
    private float waterLevel;   //How much the current water is.
    public float newWaterLevel; //Difference for the fillspeeddisplay
    public float useAmount; //how much water is used
    private bool hasWater;  //Makes sure the player has water.
    [SerializeField] private float[] UpgradeFillSpeed;  //The upgrades level for the fillspeed.
    void Start()
    {
        waterLevel = 0;
        newWaterLevel = 100;
    }

    void Update()
    {

        if (waterLevel != newWaterLevel)
        {
            UpdateWaterMeter();
            WaterText();
        }
        if (waterLevel <= 100 && waterLevel > 10)
            hasWater = true;
        else
            hasWater = false;

        AddWater(fillSpeed * Time.deltaTime);
    }



    private void UpdateWaterMeter()
    {
        //UPDATE WATERLEVEL
        if (waterLevel < newWaterLevel && waterLevel > newWaterLevel - 2)   //Sets the waterlevel to the new when it inside a small margin.
            waterLevel = newWaterLevel;
        else if (waterLevel < newWaterLevel)    //adds more water
            waterLevel += fillSpeedDisplay * Time.deltaTime;

        if (waterLevel > newWaterLevel && waterLevel < newWaterLevel + 2)   //Sets the waterlevel since its inside a margin.
            waterLevel = newWaterLevel;
        else if (waterLevel > newWaterLevel)    //removes water
            waterLevel -= fillSpeedDisplay * Time.deltaTime;
        else if (waterLevel > 100)  //Makes sure water dosent go over 100%
            waterLevel = 100;


        //DISPLAY WATERLEVEL
        if (waterLevel <= 105 && waterLevel > 50)   //Waterlevel in the left tank.
        {
            waterMeterLeft.fillAmount = (waterLevel - 50) / 50; //Converts the waterlevel to be between 1 -> 0
            waterMeterRight.fillAmount = 1; //sets the right tank to be full, fast filling speed made it stop at 75% etc.
        }
        else if (waterLevel <= 50f) //Fills the right tank. Same function as above. 
        {
            waterMeterLeft.fillAmount = 0;
            waterMeterRight.fillAmount = (waterLevel) / 50;
        }
        else if (waterLevel >= 100) //Makes sure both tanks a filled up if its full. small gaps did happen.
        {
            waterMeterLeft.fillAmount = 1;
            waterMeterRight.fillAmount = 1;
        }

    }

    /// <summary>
    /// Changes the numbers that shows the percentage of the waterlevel.
    /// </summary>
    private void WaterText()
    {
        if (waterLevel > 99)    //sets to 100%
        {
            counter[0].enabled = true;
            counter[0].sprite = numbers[1];
            counter[1].sprite = numbers[0];
            counter[2].sprite = numbers[0];
        }
        if (waterLevel <= 99)
        {
            counter[0].enabled = false;
            counter[1].sprite = numbers[(int)(Mathf.Clamp((waterLevel / 10), 0, 9))];   //Clamped to make sure it does not reach 10.
            counter[2].sprite = numbers[(int)(Mathf.Abs(waterLevel % 10))]; //array got our of reach. fixed with absolute value. 
        }
    }

    public void UseWater()
    {
        newWaterLevel -= useAmount;
        if (newWaterLevel < 0)
            newWaterLevel = 0;
    }

    public void AddWater(float amount)
    {
        newWaterLevel += amount;
        if (newWaterLevel > 100)
            newWaterLevel = 100;
    }

    public void FillTank()
    {
        newWaterLevel = 100;
    }

    public void SetWaterUpgrade(int upgradeLevel)
    {
        if(upgradeLevel > UpgradeFillSpeed.Length)
        {
            Debug.LogWarning("Level set higher than array "/* + gameObject.name*/, gameObject);
            return;
        }
        fillSpeed = UpgradeFillSpeed[upgradeLevel];
    }

        public void SetWaterMeter(float volume)
    {
        waterLevel = volume;
    }

        public bool HasWater()
    {
        return hasWater;
    }

}
