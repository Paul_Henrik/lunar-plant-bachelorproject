﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;

public class Clock : MonoBehaviour
{
    #region Variables
    [Range(0.1f, 10f)]
    public float timeFlow = 20;   //How fast the time should pass. Not sure how to make a algorithm to make a day go in X amount of minutes.
    public InteractObject playerInteract;

    //Visual variables
    public GameObject rotateFiller; //The filler that will rotate around the clock (No pun intended)
    public GameObject greyFiller; //Filler that hides the rotatefiller during the first tick
    public GameObject[] fillers = new GameObject[12]; //Array that holds the full fillers
    int fillersActive = 0; //How many fillers have been set to active
    public Image[] dayCounter = new Image[3]; //The images that make up the day counter. These are the images that will change every day
    public Sprite[] numbers = new Sprite[10]; //The number sprites used for the day counter

    [SerializeField]
    private float minutes = 0;    //is multiplied with Time.deltatime; 1sec = 1min
    private float minutesChanged; //To prevent unnecessary trying to set values.
    [SerializeField]
    private int hours = 0;      //Hours. 24 or whatever wished for in a day
    [SerializeField]
    private int days = 0;       //How many days have passed
    [SerializeField]
    private Vector3 timeStamp;  //x = minutes, y = hours, z = days. Uses a vector since it easier to send it around to other scripts (i think). 
    public bool pauseTime = false;  //pauses the time.
    private int addMinutes;
    private int addHours;
    private int addDays;
    [Range(0.1f, 20f)]
    public float timeScale;     //How fast ingame time is calculated.
    
    #endregion
    
    void Start()
    {
        minutesChanged = hours;
        dayCounter[1].gameObject.SetActive(false);
        dayCounter[2].gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (!pauseTime)
            updateTime();   //keeps the clock turning. by adding a second per second. (Time.deltaTime)
        Time.timeScale = timeScale;

        //if (Input.GetKeyDown(KeyCode.F))
        //    timeScale = (Time.timeScale == 20) ? 1 : 20;   //Changes timescale between 20 and 1. For debugging and reduce wait time.
    }

    #region Functions
    /// <summary>
    /// <para> Updates the time per second.</para>
    /// </summary>
    void updateTime()   //updates the time.
    {
        minutes = minutes + (timeFlow * Time.deltaTime);
        if (minutes >= 60)  //Adds hours per  60 minutes
        {
            minutes = 0;    //resets minutes
            hours++;

            if(hours % 2 == 0) //Activates a new tick every 2 hours, since there is 24 hours in a day and 12 filler fields on the clock
            {
                fillers[fillersActive].SetActive(true); //Sets the next filler to active/visible
                greyFiller.SetActive(false); //Hides the grey filler, as it is only needed for the first tick
                fillersActive++; //Move on to the next inactive filler

                //Reset the rotateFiller. It starts at 15 degrees, and move 30 degrees for each filled tick. It is then reset to that position to avoid it rotating to far.
                //To avoid miscalculations and misrepresentation of time
                rotateFiller.transform.rotation = Quaternion.AngleAxis(15 - 30 * fillersActive, new Vector3(0, 0, 1));
            }
        }
        if (hours >= 24)     //adds day per 24 hours
        {
            hours = 0;      //resets hours
            days += 1;

            foreach(GameObject filler in fillers) //Resets all the fillers, so they are not visible anymore
            {
                filler.SetActive(false);
            }

            greyFiller.SetActive(true); //Activates the grey filler to hide the rotation filler again
            fillersActive = 0; //Resets the active fillers indicator

            if(days < 10) //if there is less than 10 days, only the first number needs to be changed
            {
                dayCounter[0].sprite = numbers[days];
            }
            else if (days < 100)//if we're between 10 and 99 days, we need to change 2 numbers. 
            {
                if (!dayCounter[1].gameObject.activeSelf)
                    dayCounter[1].gameObject.SetActive(true);

                int tempDays = days; //Will end up holding the first number
                int counter = 0; //Will end up holding the second number

                for(int i = 0; i < 10; i++)
                {
                    if(tempDays < 10)
                    {
                        break;
                    }

                    counter++; //Used to determine which number will be the second number. (f.eks. in 10, the second number is 1 and the first is 0)
                    tempDays -= 10; //Reduces the second number by one, then checks again when the loop starts over
                }
                //When calculations are done, update the actual sprites
                dayCounter[0].sprite = numbers[tempDays];
                dayCounter[1].sprite = numbers[counter];
            }
            else
            {
                if (!dayCounter[2].gameObject.activeSelf)
                    dayCounter[2].gameObject.SetActive(true);

                int tempDays = days; //Will end up holding the first number
                int counterTen = 0; //Will end up holding the second number
                int counterHundred = 0; //Will end up holding the third number

                for (int i = 0; i < 10; i++) //Same as the previous loop, but for the third number (100) instead of the second (10)
                {
                    if (tempDays < 100)
                    {
                        break;
                    }

                    counterHundred++; //Determines the third number
                    tempDays -= 100;
                }
                for (int i = 0; i < 10; i++)
                {
                    if (tempDays < 10)//After determining the third number, we rerun this loop to determine the second number
                    {
                        break;
                    }

                    counterTen++; //Determines the second number
                    tempDays -= 10;
                }
                //Whatever is left determines the first number. Then we set the different numbers with sprites: 
                dayCounter[0].sprite = numbers[tempDays];
                dayCounter[1].sprite = numbers[counterTen];
                dayCounter[2].sprite = numbers[counterHundred];
            }
        }

        //Updates a vector3 with minutes
        //Used vector3 since its easier than making a struct, class or whatever. 
        if (minutes != minutesChanged + 1)
        {
            timeStamp.x = (int) minutes;      //assign variables
            timeStamp.y = (int) hours;
            timeStamp.z = (int) days;
            minutesChanged = minutes;

            rotateFiller.transform.Rotate(0, 0, (-0.5f * Time.deltaTime* 1.25f));
        }

    }

    /// <summary>
    /// Adds time to the clock.
    /// </summary>
    /// <param name="_minutes"></param>
    /// <param name="_hours"></param>
    public void AddTime(int _minutes, int _hours, int _days)
    {

        //Thought for later: If added time makes a year pass it needs its own if function.

        //Calculates minutes, hours and day if a minute pass it. Goes through the process twice to
        //calculate that hours and minutes dosen't go over 60minutes. also I have a bad coding day.. so its made with IFs.
        do
        {
            {
                if (_hours >= 24)       //Makes hours into days
                {
                    addDays += _hours / 24;
                    addHours += _hours % 24;
                    _hours = _hours / 24;
                }
                else if (addHours >= 24)    //make hours into days again
                {
                    addDays += addHours / 24;
                    addHours += addHours % 24;
                }
                if (_minutes >= 60) //Make minutes into hours
                {
                    addHours += _minutes / 60;
                    addMinutes += _minutes % 60;
                    _minutes = _minutes / 60;
                }
                else if (addMinutes >= 60)  //Make minutes into hours again
                {
                    addHours += addMinutes / 60;
                    addMinutes += addMinutes % 60;
                }
            }
            minutes += addMinutes;
            hours += addHours;
            days += addDays;

            if (minutes > 60)
            {
                hours += (int)minutes / 60;
                minutes += minutes % 60;
            }
            if (hours > 24)
            {
                days += hours / 24;
                hours = hours % 24;
            }
        } while (hours > 24 && minutes > 60);
    }

    /// <summary>
    /// If the clock starts at something other than 0, this function will update the clock visuals
    /// to start at the set time. This includes updating days and the slider around the clock.
    /// </summary>
    /// <param name="tempTimeStamp"> The time the clock visuals are set to represent </param>
    public void SetClockVisuals(Vector3 tempTimeStamp)
    {
        int tempMinutes = (int)tempTimeStamp.x;
        int tempHours = (int)tempTimeStamp.y;
        int tempDays = (int)tempTimeStamp.z;

        days = tempDays;
        hours = tempHours;
        minutes = tempMinutes;
        
        dayCounter[1].gameObject.SetActive(false);
        dayCounter[2].gameObject.SetActive(false);

        //SET DAYS
        if (tempDays < 10) //if there is less than 10 days, only the first number needs to be changed
        {
            dayCounter[0].sprite = numbers[tempDays];
        }
        else if (tempDays < 100)//if we're between 10 and 99 days, we need to change 2 numbers. 
        {
            if (!dayCounter[1].gameObject.activeSelf)
                dayCounter[1].gameObject.SetActive(true);
            
            int counter = 0; //Will end up holding the second number

            for (int i = 0; i < 10; i++)
            {
                if (tempDays < 10)
                {
                    break;
                }

                counter++; //Used to determine which number will be the second number. (f.eks. in 10, the second number is 1 and the first is 0)
                tempDays -= 10; //Reduces the second number by one, then checks again when the loop starts over
            }
            //When calculations are done, update the actual sprites
            dayCounter[0].sprite = numbers[tempDays];
            dayCounter[1].sprite = numbers[counter];
        }
        else
        {
            if (!dayCounter[2].gameObject.activeSelf)
                dayCounter[2].gameObject.SetActive(true);
            
            int counterTen = 0; //Will end up holding the second number
            int counterHundred = 0; //Will end up holding the third number

            for (int i = 0; i < 10; i++) //Same as the previous loop, but for the third number (100) instead of the second (10)
            {
                if (tempDays < 100)
                {
                    break;
                }

                counterHundred++; //Determines the third number
                tempDays -= 100;
            }
            for (int i = 0; i < 10; i++)
            {
                if (tempDays < 10)//After determining the third number, we rerun this loop to determine the second number
                {
                    break;
                }

                counterTen++; //Determines the second number
                tempDays -= 10;
            }
            //Whatever is left determines the first number. Then we set the different numbers with sprites: 
            dayCounter[0].sprite = numbers[tempDays];
            dayCounter[1].sprite = numbers[counterTen];
            dayCounter[2].sprite = numbers[counterHundred];
        }

        //SET HOURS
        if(tempHours % 2 == 1 && tempHours != 0)
        {
            tempHours--;
            tempMinutes += 60;
        }

        foreach(GameObject filler in fillers)
        {
            filler.SetActive(false);
        }

        for(int i = 0; i < tempHours/2; i++)
        {
            fillers[i].SetActive(true);
        }

        fillersActive = tempHours / 2;

        //SET MINUTES
        float progressIntoNextFiller = (float)tempMinutes / 120;

        rotateFiller.transform.rotation = Quaternion.AngleAxis(15 - 30 * fillersActive - 30*progressIntoNextFiller, new Vector3(0, 0, 1));

        timeStamp = tempTimeStamp;
    }

    /// <summary>
    /// Checks that minutes and hours are restricted according to time rules.
    /// <para>Returns a vector3</para>
    /// </summary>
    /// <param name="inputTime"></param>
    /// <returns></returns>
    public static Vector3 ReformatToTime(Vector3 inputTime)
    {
        //There is a bug if you send in 1440 minutes. 
        //Timeformat is the time that will be returned. 
        Vector3 timeFormat = new Vector3(0, 0, 0);
        //X = MINUTES
        //Y = HOURS
        //Z = DAYS
        
        for (int i = 0; i < 3; i++)
        {
            if (inputTime.y >= 24)       //Makes hours into days
            {
                timeFormat.z += (int)inputTime.y / 24;
                timeFormat.y += (int)inputTime.y % 24;
                inputTime.y = (int)inputTime.y / 24;
                inputTime.y = 0;
            }
            else if (timeFormat.y >= 24)    //make hours into days again
            {
                timeFormat.z += (int)timeFormat.y / 24;
                timeFormat.y += (int)timeFormat.y / 24;
                timeFormat.y += (int)inputTime.y % 24;
            }
            else if (timeFormat.y == 0)
                timeFormat.y = inputTime.y;

            if (inputTime.x >= 60) //Make minutes into hours
            {
                timeFormat.y += (int)inputTime.x / 60;
                timeFormat.x += (int)inputTime.x % 60;
                inputTime.x = 0;
            }
            else if (timeFormat.x >= 60)  //Make minutes into hours again
            {
                timeFormat.y += (int)timeFormat.x / 60;
                timeFormat.x += (int)timeFormat.x / 60;
                timeFormat.x += (int)inputTime.x % 24;
            }
            else if(timeFormat.x == 0)
                timeFormat.x = inputTime.x;

            if (inputTime.z >= 1)
            {
                timeFormat.z += inputTime.z;
                inputTime.z = 0;
            }
            

        }

        return timeFormat;
    }

    /// <summary>
    /// Returns a vector3 with current time.
    /// </summary>
    /// <returns>Vector3(minutes, hours, days)</returns>
    public Vector3 getTime()
    {
        return timeStamp;
    }

    public void PauseTime(bool set)
    {
        pauseTime = set;
    }
    
    #endregion
}