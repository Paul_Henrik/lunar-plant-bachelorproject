﻿using System;
using UnityEngine;
using UnityEngine.Assertions;

//This is the scene manager
public class MainManager : MonoBehaviour
{
    [Range(0, 100), SerializeField]
    private float waterTime;             //How long a tile will be watered for
    [Range(0, 4), SerializeField]
    private float SkyboxRotationSpeed;   //How fast the skybox should rotate
    [SerializeField]
    private GameObject selectedObject;   //object clicked on //uses InteractObject script
    [SerializeField]
    private GameObject[] plantPrefab = new GameObject[6];    //used for loading. #CHECK THIS KAYA
    private GameObject plantObject;     //Making a new object from the prefab
    [SerializeField]
    private GameObject marketplaceMenu; //Holds the marketplace to be able to turn it on and off
    public bool[] discoveredPlants = new bool[6]; //List over discovered plants by enum index in Seed.Type
    public bool overlayOpen = false; //If any kind of overlay is covering most of the screen, this bool is set to true

    //Referances                        //Makes the code easier to read.
    private GameObject player;           //Referance to player.
    private PlayerInventory inventory;   //Referance to inventory.
    [SerializeField]
    private Tutorial tutorial; //Referance to the tutorial script
    private Clock clock; //Referance to the clock script (on this game object)
    [SerializeField]
    private SettingsMenu settingsMenu; //Referance to the settings menu script

    //For freezing and unfreezing camera
    public Cinemachine.CinemachineFreeLook camRig;
    float sensitivityX;
    float sensitivityY;

    //For sound management
    public AudioSource[] backgroundMusic = new AudioSource[8];
    public AudioSource[] spaceshipSound = new AudioSource[2];
    private int nextBackgroundTrack = 0, nextSpaceshipTrack = 0;

    //For world and environment animation
    public Animator spaceshipAnim;


    //For save and load
    public WorldData data;
    private string saveLoadPath;

    //Sets cursor image
    public Texture2D cursorImage;
    
    void Awake()
    {
        data = new WorldData();
        saveLoadPath = System.IO.Path.Combine(Application.persistentDataPath, "Save1.json");
        player = GameObject.FindGameObjectWithTag("Player");
        clock = GetComponent<Clock>();
        inventory = player.GetComponent<PlayerInventory>();
    }

    /// <summary>
    /// Gets necessary objects.
    /// Makes cursor invisible and locks it to the bounds of the screen.
    /// </summary>
    void Start()
    {
        Assert.IsNotNull(plantPrefab, "MainManager does not have a plant Prefab");
        Assert.IsNotNull(player, "MainManager does not have the player referance");

        sensitivityX = camRig.m_XAxis.m_MaxSpeed;
        sensitivityY = camRig.m_YAxis.m_MaxSpeed;

        discoveredPlants[0] = true; //The player has always known about the commonplant

        Cursor.SetCursor(cursorImage, new Vector2(0.2f, 0), CursorMode.Auto);

        if(SaveLoadManager.loadScene) //If the game is loaded, and not a new game, it loads the information from the save file
        {
            SaveLoadManager.Load(saveLoadPath);
            SaveLoadManager.loadScene = false;
        }
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.T) && !clock.pauseTime) //Opens the markedplace menu.
        {
            marketplaceMenu.SetActive(!marketplaceMenu.activeSelf);
        }

        if(!backgroundMusic[nextBackgroundTrack].isPlaying)
        {
            GetNewBackgroundTrack();
        }

        if(!spaceshipSound[nextSpaceshipTrack].isPlaying)
        {
            GetNewSpaceshipTrack();
        }

        RotateSkybox();
    }

    /// <summary>
    /// Sets a new background track each time the previous track is finished playing
    /// </summary>
    private void GetNewBackgroundTrack()
    {
        nextBackgroundTrack = UnityEngine.Random.Range(0, 4);
        backgroundMusic[nextBackgroundTrack].Play();
    }

    /// <summary>
    /// Sets background ambient for the spaceship
    /// </summary>
    private void GetNewSpaceshipTrack()
    {
        nextSpaceshipTrack = UnityEngine.Random.Range(1, 2);
        spaceshipSound[nextSpaceshipTrack].Play();
        spaceshipAnim.SetInteger("Idle", nextSpaceshipTrack);
    }
    

    /// <summary>
    /// Water all the tiles on the whole asteroid
    /// </summary>
    public void WaterAllTiles()
    {
        BasePlant[] targets = FindObjectsOfType(typeof(BasePlant)) as BasePlant[];
        foreach (var item in targets)
        {
            item.WaterPlant();
        }
    }

    /// <summary>
    /// Plant a common plant on all the available tiles on the whole asteroid
    /// </summary>
    public void ActivatePlanted()
    {
        BasePlant[] targets = FindObjectsOfType(typeof(BasePlant)) as BasePlant[];
        foreach (var item in targets)
        {
            if(!item.GetPlanted())
            {
                item.Planted();
            }
        }
    }

    private void RotateSkybox()
    {
        RenderSettings.skybox.SetFloat("_Rotation", Time.time * SkyboxRotationSpeed);
    }

    public void InstantiatePlantPrefab(Seed.Type type, Vector3 pos, Quaternion rot, PlantData newData) //Used for loading plants from the save file
    {
        GameObject blueprint;
        switch (type)
        {
            case Seed.Type.Normal:
                blueprint = plantPrefab[0];
                break;
            case Seed.Type.Shield:
                blueprint = plantPrefab[1];
                break;
            case Seed.Type.Water:
                blueprint = plantPrefab[2];
                break;
            case Seed.Type.Light:
                blueprint = plantPrefab[3];
                break;
            case Seed.Type.Fruit:
                blueprint = plantPrefab[4];
                break;
            default:
                return;
        }

        plantObject = Instantiate(blueprint, pos, rot); //makes the object a prefab
        plantObject.GetComponent<BasePlant>().data = newData;
    }

    /// <summary>
    /// Freezes the camera rotation. Used when an overlay is open
    /// </summary>
    public void FreezeCam()
    {
        camRig.m_XAxis.m_MaxSpeed = 0;
        camRig.m_YAxis.m_MaxSpeed = 0;
    }

    /// <summary>
    /// Unfreezes the camera rotation
    /// </summary>
    public void UnfreezeCam()
    {
        camRig.m_XAxis.m_MaxSpeed = sensitivityX;
        camRig.m_YAxis.m_MaxSpeed = sensitivityY;
    }

    /// <summary>
    /// Sets the mouse sensitivity in the x axis (horizontal). Used from the settings menu
    /// </summary>
    /// <param name="newSens"></param>
    public void SetSensX(float newSens)
    {
        sensitivityX = newSens;
    }

    /// <summary>
    /// Sets the mouse sensitivity in the y axis (vertical). Used from the settings menu
    /// </summary>
    /// <param name="newSens"></param>
    public void SetSensY(float newSens)
    {
        sensitivityY = newSens;
    }

    public float GetSensX()
    {
        return sensitivityX;
    }

    public float GetSensY()
    {
        return sensitivityY;
    }

    public void SaveGame()
    {
        SaveLoadManager.Save(saveLoadPath, SaveLoadManager.containers);
    }

    public void ClosedOverlay()
    {
        overlayOpen = false;
    }

    #region SaveLoad

    public void ApplyOnetimeData(DataContainers newData)
    {
        inventory.data = newData._inventoryData[0];
        this.data = newData._worldData[0];
        settingsMenu.data = newData._settingsData[0];
    }

    void OnEnable()
    {
        SaveLoadManager.OnLoaded += LoadData;
        SaveLoadManager.OnBeforeSave += StoreData;
        SaveLoadManager.OnBeforeSave += ApplyData;
    }

    void OnDisable()
    {
        SaveLoadManager.OnLoaded -= LoadData;
        SaveLoadManager.OnBeforeSave -= StoreData;
        SaveLoadManager.OnBeforeSave -= ApplyData;
    }

    public void StoreData()
    {
        data._playerPos = player.transform.position;

        data._timeStamp = clock.getTime();

        data._showTips = tutorial.GetShowTips();
        data._marketShown = tutorial.marketShown;
        data._crocusDiscovered = tutorial.crocusFound;

        for (int i = 0; i < 6; i++)
        {
            data._discoveredPlants[i] = discoveredPlants[i];
        }

        bool[] temp = player.GetComponent<PlayerUpgrades>().allUpgrade;

        for(int i = 0; i < temp.Length; i++)
        {
            data._upgradesBought[i] = temp[i];
        }
    }

    public void LoadData()
    {
        player.transform.position = data._playerPos;

        clock.SetClockVisuals(data._timeStamp);

        tutorial.SetShowTips(data._showTips);
        tutorial.crocusFound = data._crocusDiscovered;
        tutorial.marketShown = data._marketShown;

        for(int i = 0; i < 6; i++)
        {
            discoveredPlants[i] = data._discoveredPlants[i];
        }

        player.GetComponent<PlayerUpgrades>().LoadLevel(data._upgradesBought);
    }

    public void ApplyData()
    {
        SaveLoadManager.AddData(data);
    }

    #endregion
}

[Serializable]
public class WorldData
{
    //Player
    public Vector3 _playerPos;

    //Clock
    public Vector3 _timeStamp;

    //Tutorial
    public bool _showTips;
    public bool _crocusDiscovered;
    public bool _marketShown;

    //Here
    public bool[] _discoveredPlants = new bool[6];

    //Upgrades
    public bool[] _upgradesBought = new bool[8];
}
