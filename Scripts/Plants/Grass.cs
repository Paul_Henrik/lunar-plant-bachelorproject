﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;

public class Grass : MonoBehaviour
{
	public List<Transform> plants;  //Current positions that will grow grass
	public Vector4[] test;  //Debugging.
    [SerializeField]
    private float timer; //So the grass don't try to update during solarstorms or every frame.
	private float resetTimer;   //The first set timer.
    [SerializeField]
	private Material mat;
	private Renderer rend;

	void Start()
	{
		resetTimer = timer;
		UpdateShader();
	}

	void Update()
	{
		timer -= Time.deltaTime;
		if (GetComponent<SolarStorm>().GetSolarStorm())  //Incase plants die during a solarstorm
		{
			timer = 2f;
		}
		if (timer < 0)
		{
			UpdateShader();
		}
	}

    /// <summary>
    /// Send in grass positions to the ground shader
    /// </summary>
	public void UpdateShader()
	{
		CheckList();

		if (plants.Count >= 100)
			return;

		Vector4[] points = new Vector4[100];    //A shader can have a max array of 100.


		for (int i = 0; i < plants.Count; i++)
		{
			points[i].x = plants[i].transform.position.x;
			points[i].y = plants[i].transform.position.y;
			points[i].z = plants[i].transform.position.z;
			points[i].w = plants[i].GetComponent<BasePlant>().radius;
		}

		mat.SetVectorArray("_Points", points);  //Positon and radius of plants.
		mat.SetInt("_Points_Length", plants.Count); //Sets how long a for loop in the shader will be.
		timer = resetTimer;
	}

    /// <summary>
    /// Adds a new plant to the grass list.
    /// </summary>
    /// <param name="input"></param>
	public void AddToPlantList(Transform input)
	{
		CheckList();
		plants.Add(input);
		UpdateShader();
		//debug();
	}

    /// <summary>
    /// Check if some plants are dead, harvested, mutated.
    /// </summary>
	public void CheckList()
	{
		for (int i = 0; i < plants.Count; i++)
		{
			if (plants[i].transform == null)
			{
				plants.RemoveAt(i);
			}
		}
		//plants.Sort();
		mat.SetFloat("_Points_Length", plants.Count);
	}

    /// <summary>
    /// Debug the shader list.
    /// <para>Perfomance Heavy</para>
    /// </summary>
	public void Debug()
	{
		Vector4[] debug = mat.GetVectorArray("_Points");
		foreach (var item in debug)
		{
            UnityEngine.Debug.Log("Shader Array: " + item);
		}
        UnityEngine.Debug.Log("Shader length: " + mat.GetInt("_Points_Length"));

        UnityEngine.Debug.Log("Plants in list: " + plants.Count);
	}
}
