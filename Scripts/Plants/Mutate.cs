﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

//The mutation chance is based on the plants being sorted after droprate MutationChancePlant[0] is highest
//in the inspector.

public class Mutate : MonoBehaviour
{
	public GameObject[] mutationPrefabs;    //Prefabs for the diffrent mutations the plant can mutate into
	[Range(0f, 100f)]
	public float mutationChance;    //The chance this plant have to mutate.
	public float[] mutationChancePlant;          //The chance to mutate to different plants.
	private float mutationRoll;     //Saves a roll between 1 and 100
	private int currentMutation;    //current mutation selected by roll. 
	private int amountMutations;    //The size of mutationChances.
	private GameObject mutationObject;  //The object created from a mutation based on prefab.
	public bool mutated = false;    //So the plant can't mutate multiple times.
	public bool plantProtected = false; //Used with shield plant
	private BasePlant basePlant;

    public AudioSource mutationSound;

	void Awake()
	{
		Assert.IsNotNull(mutationPrefabs, "Plant have no mutationPrefab to mutate into");
		amountMutations = mutationPrefabs.Length;   //Gets the amount of diffrent mutations the plant can have.
													//Gives a warning if mutationPrefabs is not equal to mutationChance.
		Assert.AreEqual(amountMutations, mutationChancePlant.Length, "MutationPrefab Array and Mutation Chance Array is not equal on: " + gameObject.name);
		basePlant = GetComponent<BasePlant>();
            
	}

	/// <summary>
	/// Tries to mutate the plant or kills it.
	/// </summary>
	public void TryToMutate()       //Assigned in script <work in progres>. For now as a button
	{
		if (!plantProtected && !mutated) //not protected by a shieldplant
		{
			if (basePlant.fullgrown == true)    //Can mutate if its fullgrown
			{
				mutationRoll = MutationRoll();    //Rolls a random number between 0-100 and then tweets about it.

				if (mutationRoll >= 100 - mutationChance)
				{
					StartMutation(PickMutation());    //Start to mutate the plant. (Spawn new plant and delete the other
				}
				else
				{
					basePlant.killPlant(0); //Kills this plant with a delay of two seconds
				}
			}
			else
			{
				basePlant.killPlant(0); //Kills this plant with a delay of two seconds
			}
		}

	}

    //Starts muating a plant
	private void StartMutation(int mutationPlant)
	{
		mutated = true;

        if (mutationSound != null)
            mutationSound.Play();
        else
            Debug.LogWarning("MutationSound not working.. AGAIN!!!! " + gameObject.name, gameObject);
		StartCoroutine(Mutating(mutationPlant));
	}

    /// <summary>
    /// Rolls to get a mutation.
    /// </summary>
    /// <returns></returns>
	private float MutationRoll()
	{
		float D100D10 = Random.Range(0f, 100f);
		return D100D10;
	}

    //Picks a mutation with a new roll.
	private int PickMutation()
	{
		mutationRoll = MutationRoll();
        float result = 0;
		for (int i = 0; i < mutationChancePlant.Length; i++)
		{
            result += mutationChancePlant[i];
            if (mutationRoll < result)
			{
				currentMutation = i;
                return currentMutation;
			}
		}

		//Debug.Log("MutationRoll: " + mutationRoll + " PlantMutation: " + currentMutation);
		return currentMutation;
	}

    /// <summary>
    /// Changes the plant to the new mutation.
    /// </summary>
    /// <param name="_mutationPlant"></param>
    /// <returns></returns>
	private IEnumerator Mutating(int _mutationPlant)
	{
		yield return new WaitForSeconds(1f);
		mutationObject = Instantiate(mutationPrefabs[_mutationPlant], transform.position, transform.rotation);  //spawns the mutated plant
		mutationObject.GetComponent<BasePlant>().Planted();
		FindObjectOfType<MainManager>().GetComponent<Grass>().plants.Add(mutationObject.transform);
		//Debug.Log(mutationObject.name);   
		basePlant.transform.GetChild(0).GetComponent<SkinnedMeshRenderer>().enabled = false;
		yield return new WaitForSeconds(2f);
		basePlant.killPlant(0.2f); //Kills the old plant
	}
}
