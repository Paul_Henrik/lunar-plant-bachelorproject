﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class ShieldPlant : BasePlant
{
	public List<GameObject> shieldedPlants = new List<GameObject>();
	[SerializeField]
	private Collider col;
	[SerializeField]
	public LayerMask interactable;
	public GameObject visualShield;

    public AudioSource shieldSound;


	new void Awake()
	{
		base.Awake();
		Assert.IsNotNull(visualShield, "Shield is null at: " + transform.name);
		GetComponent<Mutate>().plantProtected = true;
		visualShield.SetActive(false);
	}
	private void Start()   //Set new cause I wants it use BasePlants Start.. or something
	{
        shieldSound = GetComponentInChildren<AudioSource>();
		GetComponent<Mutate>().plantProtected = true;
	}

    /// <summary>
    /// Protects nearby plants
    /// </summary>
    private void ProtectedPlants()
	{
		Collider[] protectedPlants = Physics.OverlapSphere(transform.position, radius, interactable);
		if (protectedPlants.Length != shieldedPlants.Count && protectedPlants.Length != 0)
		{
			shieldedPlants.Clear();
			foreach (Collider col in protectedPlants)
			{
                
				Vector3 distance = col.transform.position - transform.position;
				if (distance.magnitude < radius + 1)
				{
                    if (!col.CompareTag("Plant"))   //Since seeds and collectables is interactable this is needed. 
                        continue;
					Mutate mutate = col.gameObject.GetComponent<Mutate>();
					if (mutate)
						mutate.plantProtected = true;
					else
                    {
						Debug.LogWarning("SOMTHING WENT WRONG HERE?!?!?!? FIX ME TOMORROW" + gameObject.name, gameObject);
                        continue;
                    }
					shieldedPlants.Add(col.gameObject);
				}
			}
		}
	}

	public override IEnumerator PlantDying(float waitTime)
	{
		died = true;

		yield return new WaitForSeconds(waitTime);

		Destroy(gameObject);
	}

	protected override void FullGrown()
	{
		plantState = State.Harvestable;
		visualShield.SetActive(true);
        col.enabled = true;
	}

    //The plants special ability
	protected override void Special()
	{
		ProtectedPlants();
        if(!shieldSound.isPlaying && fullgrown)
        {
            shieldSound.Play();
        }
	}

    /// <summary>
    /// Removes it shield from nearby plants
    /// </summary>
	private void OnDestroy()
	{
		foreach (GameObject plant in shieldedPlants)
		{
			if (plant == null)
				continue;
			else if (!plant.GetComponent<ShieldPlant>())
				plant.GetComponent<Mutate>().plantProtected = false;
		}
	}

    public override void Planted()
    {
        base.Planted();
        visualShield.SetActive(true);
    }
}

