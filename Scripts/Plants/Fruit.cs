﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fruit : BasePlant
{
    private bool newFruit;  //When a fruit is finished a new fruit starts growing.
    private FruitPlant fp;  //Grow new fruits, get waterlevel.
    private bool clonedFruit; //changes to interactable after x seconds.
    public Mesh fullGrownMesh;   //Changing model when its fullgrown.
    private Mesh growingMesh;
    public float maxSize;

    protected override void Awake()
    {
        fp = GetComponentInParent<FruitPlant>();
        plantMaxSize = transform.localScale;
        universalClock = GameObject.FindGameObjectWithTag("Manager").GetComponent<Clock>();
        rend = GetComponent<Renderer>();
        growingMesh = transform.GetChild(0).GetComponent<MeshFilter>().mesh;
    }


    protected override void Update()
    {
        if (clonedFruit)
        {
            StartCoroutine(SwitchToInteractable());
            clonedFruit = false;
        }
        base.Update();

    }

    /// <summary>
    /// Removed the lines that kills the plant. It just stops growing instead.
    /// </summary>
    protected override void Watered()
    {
        waterLevel = fp.GetWaterLevel();
        if (waterLevel > 40)
        {
            if (VectorIsSmaller(transform.localScale, plantMaxSize)) //Makes sure the plant grow over its max size
                transform.localScale += new Vector3(growthRate, growthRate, growthRate) * Time.deltaTime;  //TEMPORARY GROWTH FOR FEEDBACK; Find a way to make it grow accordingly to next stage.

            plantState = State.Growing;

            if (paused) //Unpauses the growth and set the plants new finnish time.
            {
                plantFinishedAt = plantFinishedAt + AbsoluteValueVector(universalClock.getTime() - pausedAt);   //Gets the absolute value since the vector can be minus
                plantFinishedAt = Clock.ReformatToTime(plantFinishedAt);
                paused = false;
            }
        }
        else
        {
            if (!paused)    //Stops the plants growth until its watered again.
            {
                plantState = State.Dehydrated;  //Feedback to player
                paused = true;
                pausedAt = universalClock.getTime();
            }
            else if (waterLevel < 0)   //Kills the plant
            {
                plantState = State.Dead;    //Feedback to player
                waterLevel = 0;
            }
        }
        waterLevel -= Time.deltaTime * waterUsage;  //Reduces the waterlevel
    }

    public override void HarvestPlant(float killPlantTime)
    {
        if (!harvested)
        {
            harvested = true;   //So it can't be harvested several times.
            CloneFruit();
            gameObject.SetActive(false);
            fullgrown = false;
        }
    }

    public override void Planted()
    {
        base.Planted();
        transform.localScale = startSize;
        newFruit = false;
        transform.GetChild(0).GetComponent<MeshFilter>().mesh = growingMesh;
    }

    protected override void FullGrown()
    {
        base.FullGrown();
        transform.GetChild(0).GetComponent<MeshFilter>().mesh = fullGrownMesh;

        if (!newFruit)
        {
            fp.StartGrowingFruit(); //Sets parent to grow a new fruit.
            newFruit = true;


        }
    }

    /// <summary>
    /// Clones a fruit
    /// </summary>
    private void CloneFruit()
    {
        GameObject dropFruit = Instantiate(gameObject, transform, true);
        dropFruit.AddComponent<GravityObject>();
        dropFruit.GetComponent<GravityObject>().weight = 5;
        dropFruit.AddComponent<SphereCollider>();
        dropFruit.transform.parent = null;
        dropFruit.GetComponent<Fruit>().SetClonedFruit(true);   //Why? could be a previous solution. 15.05.2018
        dropFruit.GetComponent<Fruit>().enabled = false;
        dropFruit.layer = LayerMask.NameToLayer("Interactable");
        dropFruit.tag = "Collectable";
    }

    private IEnumerator SwitchToInteractable() //Lets the fruit fall to the ground before the player is able to pick it up.
    {
        Debug.Log("running");
        yield return new WaitForSeconds(2f);
        gameObject.layer = LayerMask.NameToLayer("Interactable");
        gameObject.tag = "Collectable";
    }

    public void SetClonedFruit(bool isCloned)
    {
        clonedFruit = isCloned;
    }
}
