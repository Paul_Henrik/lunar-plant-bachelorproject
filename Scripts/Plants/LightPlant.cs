﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightPlant : BasePlant 
{
    public List<GameObject> illuminatedPlants;  //Plants that will receive the lightboost.
    [SerializeField] private LayerMask mask;  //The affected plants layer.
    public Material lightMaterial;  //Turns on the light when the plant is fullgrown.
    [SerializeField]
    private Collider plantCollider;

    private float debugCounter; //Debug list
    public override void Planted()
    {
        base.Planted();
        plantCollider.enabled = true;
    }

    new void Awake()
    {
        base.Awake();
        plantCollider.enabled = false;
    }


    /// <summary>
    /// Give surrounding plants lightbonus.
    /// </summary>
    public void IlluminatePlants()
    {
        Collider[] illuminatedPlantsCol = Physics.OverlapSphere(transform.position, radius, mask);
        if (illuminatedPlantsCol.Length != illuminatedPlants.Count)
        {
            //debugCounter++;
            //Debug.Log(debugCounter);  //Checks that it does not run every frame

            illuminatedPlants.Clear();
            for (int i = 0; i < illuminatedPlantsCol.Length; i++)
            {
                Vector3 distance = illuminatedPlantsCol[i].transform.position - transform.position;
                if (distance.magnitude < radius + 1)
                {
                    illuminatedPlants.Add(illuminatedPlantsCol[i].gameObject);
                    if (!illuminatedPlantsCol[i].CompareTag("Plant"))
                        continue;
                    if (illuminatedPlantsCol[i].GetComponent<Seed>().seedType != Seed.Type.Light)
                        illuminatedPlantsCol[i].GetComponent<BasePlant>().SetLight(true);
                }
            }
        }
    }

    //Removes the lightbonus on surrounding plants.
    private void OnDestroy()
    {
        for (int i = 0; i < illuminatedPlants.Count; i++)
        {
            if (illuminatedPlants[i] == null)
                continue;
            if(illuminatedPlants[i].CompareTag("Plant"))
                illuminatedPlants[i].GetComponent<BasePlant>().SetLight(false);
        }
    }

    protected override void Special()
    {
        IlluminatePlants();
    }

    protected override void FullGrown()
    {
        base.FullGrown();
        IlluminatePlants();
    }

}
