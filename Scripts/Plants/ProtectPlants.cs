﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Protects the plants that are next to the spaceship.
//Also refills the players watertank. 

public class ProtectPlants : MonoBehaviour
{
    PlaceableObject plants; //Add and removes plants inside the collider.
    private int listLength;
    private WaterMeter playerWater;

    void Start()
    {
        playerWater = FindObjectOfType<PlayerMovement>().GetComponent<WaterMeter>();
        plants = GetComponent<PlaceableObject>();
        listLength = 0; //Makes sure the script only run when a new plant is inside the field. 
    }

    void Update()
    {
        if (listLength > 0 && listLength != plants.colliders.Count)
        {
            foreach (var item in plants.colliders)
            {
                if (item == null)
                {
                    plants.colliders.Remove(item);
                }
                else if (item.CompareTag("Plant"))
                    item.GetComponent<Mutate>().plantProtected = true;
            }
        }
        listLength = plants.colliders.Count;
    }

    void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Player"))
            playerWater.AddWater(1);
    }

}
