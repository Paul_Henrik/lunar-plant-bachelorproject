﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class FruitPlant : BasePlant
{
    [SerializeField]
    private List<Fruit> fruits;  
    [SerializeField]
    private float fruitGrowSpeed;
    [SerializeField]
    private bool startGrowingFruits = false;
    public Collider plantCollider; //Collider that isn't a trigger

    new void Awake()
    {
        base.Awake();
        Assert.IsNotNull(plantCollider, "Plantcollider not set on: " + gameObject.name);
        plantCollider.enabled = false;
    }

    #region Functions;

    /// <summary>
    /// Starts growing a fruit
    /// </summary>
    public void GrowFruit()
    {
        foreach (var fruit in fruits)
        {
            if (fruit.gameObject.activeSelf != true)
            {
                fruit.gameObject.SetActive(true);
                fruit.Planted();
                break;
            }
        }
    }

    /// <summary>
    /// Activates the harvest collider.
    /// </summary>
    public override void Planted()
    {
        base.Planted();
        plantCollider.enabled = true;
    }

    //Starts growing fruits when the tree fullgrown.
    protected override void FullGrown()
    {
        base.FullGrown();
        waterLevel -= Time.deltaTime * waterUsage; //fruits dependent on the plants waterlevel.

        if (!startGrowingFruits)    //Starts growing fruits.
        {
            GrowFruit();
            startGrowingFruits = true;
        }


    }


    /// <summary>
    /// Harvest fruits, if there are no fruits the tree will be harvested.
    /// </summary>
    /// <param name="killPlantTime"></param>
    public override void HarvestPlant(float killPlantTime)
    {
        if(HarvestableFruits()) //if there is no grown fruits to harvest the tree will be harvested instead.
        {
            if (!harvested)
            {
                GetComponent<Seed>().SpawnSeedLoot(plantType, dropSeedAmount, fullgrown);
                killPlant(killPlantTime);
                harvested = true;   //So it can't be harvested several times.
            }
        }

    }

    /// <summary>
    /// Start growing a new fruit
    /// </summary>
    public void StartGrowingFruit()
    {
        startGrowingFruits = false;
    }


    private bool HarvestableFruits()    //Harvest one fruit from the tree. If the fruits aren't fullgrown the tree will be harvested.
    {
        bool harvestTree = true;
        foreach (Fruit fruit in fruits)
        {
            if(fruit.gameObject.activeSelf && fruit.fullgrown)
            {
                harvestTree = false;
                fruit.HarvestPlant(2);
                startGrowingFruits = CheckForGrowingFruits();
                break;
            }
        }
        return harvestTree;
    }

    /// <summary>
    /// Checks if the fruitplants have any growing fruits.
    /// </summary>
    /// <returns></returns>
    public bool CheckActiveFruits()
    {
        bool retBool = false;
        foreach (Fruit fruit in fruits)
        {
            if (fruit.gameObject.activeSelf)
            {
                retBool = true;
                break;
            }
        }
        return retBool;
    }

    /// <summary>
    /// Check if there is a fruit currently growing.
    /// </summary>
    /// <returns></returns>
    public bool CheckForGrowingFruits()
    {
        bool retBool = false;
        foreach (Fruit fruit in fruits)
        {
            if (fruit.gameObject.activeSelf && fruit.fullgrown == false)
            {
                retBool = true;
                break;
            }
        }
        return retBool;
    }
    #endregion
}
