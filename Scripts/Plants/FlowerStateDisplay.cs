﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowerStateDisplay : MonoBehaviour
{
    [SerializeField]
    private Sprite[] sprites;    // going to be changed to particle systems at some poin
    [SerializeField]
    private GameObject[] feedback;   //For the particle effects
    [Range(0, 1), SerializeField]
    private float transparancy = 0.6f;   //How transparent icons are
    private bool fadeUp; //For fading in or out function.
    private bool fading;    //If currently fading.
    private bool mutating = false;  //Makes sure mutation feedback dosent restarts.
    private bool mutatingStarted = false;   //For mutating feedback.
    [SerializeField]
    private Vector3[] newPosition = new Vector3[2]; //New position of the icon sprite
    private bool positionReached = true;
    private int curPos;     //Current arrayposition of newPosition. 
    private BasePlant bs;   //Referance
    private SpriteRenderer rend;    //Referance

    // Use this for initialization
    void Start()
    {
        bs = GetComponent<BasePlant>();
        rend = GetComponentInChildren<SpriteRenderer>();
        rend.color = new Color(1, 1, 1, transparancy);
    }

    // Update is called once per frame
    void Update()
    {
        if (!mutatingStarted)
            mutating = GetComponent<Mutate>().mutated;
        SetState(GetComponent<BasePlant>().GetPlantState());


        if (fading)
            Fading();
        if (bs.watered)
            PlantWatered();
        if (!mutatingStarted && mutating)
            PlantMutation();
        if (!positionReached)
        {
            float left = ((newPosition[curPos]) - rend.transform.localPosition).magnitude;
            //Debug.Log(left);
            if (left <= 0.1f)
            {
                curPos++;
                positionReached = true;
                return;
            }
            rend.transform.localPosition += (newPosition[curPos] - rend.transform.localPosition) * 2 * Time.deltaTime;
        }
        else
            rend.gameObject.transform.LookAt(Camera.main.transform, transform.up);

    }

    public void SetState(BasePlant.State state)
    {
        switch (state)
        {
            case BasePlant.State.Growing:   //The same as normal
                rend.sprite = sprites[0];
                //spawn particle system or soemthing
                break;
            case BasePlant.State.Dead:
                rend.sprite = sprites[1];
                //spawn skeleton sign
                break;
            case BasePlant.State.Harvestable:
                rend.sprite = sprites[2];
                break;
            case BasePlant.State.Dehydrated:
                //spawn watersign.
                rend.sprite = sprites[3];
                break;
            case BasePlant.State.Mutating:
                //spawn mutate sign.
                break;
            default:
                break;
        }
    }

    /// <summary>
    /// Sets the transparanct of the sprite renderer.
    /// <para>Must be between 0 and 1. null is invisible.</para>
    /// </summary>
    /// <param name="transparancy"></param>
    public void SetTransparancy(float transparancyLevel)
    {
        transparancy = transparancyLevel;
        rend.color = new Color(1, 1, 1, transparancy);
    }

    /// <summary>
    /// Fades the icon
    /// </summary>
    public void Fading()    //Not used, but were made to blink when waterlevel was urgently low.
    {
        if (fadeUp)
        {
            transparancy += 0.01f;
            SetTransparancy(transparancy);
            if (transparancy > 0.9f)
                fadeUp = false;
        }
        else
        {
            transparancy -= 0.01f;
            SetTransparancy(transparancy);
            if (transparancy < 0.4f)
                fadeUp = true;
        }
    }

    /// <summary>
    /// Plays the water particle system
    /// </summary>
    private void PlantWatered()
    {
        if (GetComponent<Seed>().seedType == Seed.Type.Water)   //Waterplants won't play the particle system.
            return;

        ParticleSystem[] waterSystems = feedback[0].GetComponentsInChildren<ParticleSystem>();
        foreach (var item in waterSystems)  //Water particle effect a few particle systems.
        {
            item.gameObject.SetActive(true);
            var emission = item.emission;
            emission.enabled = true;
            item.Play();
        }
    }

    /// <summary>
    /// Plays the mutation particle system
    /// </summary>
    private void PlantMutation()
    {
        mutatingStarted = true;
        feedback[1].SetActive(true);
        feedback[1].GetComponent<ParticleSystem>().Play();
    }

    /// <summary>
    /// Starts to make the feedback icon move to new position.
    /// </summary>
    public void NewPos()
    {
        positionReached = false;
    }

}
