﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Was to be used before solarstorms. 

public class ShowShield : MonoBehaviour
{
    public float normalFade;    //The normal transparancy of the shield.
    public float fadeSpeed; //How fast the shield fades in/out
    private float maxFade = 1;
    private float minFade = 0.05f;
    private Material mat;
    private MaterialPropertyBlock prop;
    public bool fadeIn; //If the shields are currently fading in or out.
    public bool fading; //IF the shield is currently fading.
    public float fadeLevel; //current fadelevel, also sends into shader.
    private Renderer rend;
    public enum Shield { FadeIn, FadeOut, Pulse, Strong }
    public Shield shield;
    private float radius;
    private float oldRadius;
    public float growSize;

    void Start()
    {
        rend = GetComponent<Renderer>();
        mat = rend.material;
        prop = new MaterialPropertyBlock();
        normalFade = mat.GetFloat("_Visibility");
        prop.SetFloat("_Visibility", minFade);
        rend.SetPropertyBlock(prop);
        StartFading(Shield.FadeIn);
        transform.localScale = new Vector3(2, 2, 2);
    }

    void Update()
    {
        if (fading)
        {
            UseFunction(shield);
        }
        radius = GetComponentInParent<ShieldPlant>().radius;
        if (radius != oldRadius)
        {
            growSize = radius * 2.1f;
            //if (transform.localScale.x < 14.72f)
                transform.localScale = new Vector3(growSize , growSize, growSize);
        }
        oldRadius = radius;
    }

    public void StartFading(Shield input)
    {
        shield = input;
        fading = true;
    }

    private void FadingIn()
    {
        if (fadeLevel < normalFade)
        {
            fadeLevel += fadeSpeed * Time.deltaTime;
            prop.SetFloat("_Visibility", fadeLevel);
            rend.SetPropertyBlock(prop);
        }
        else if (fadeLevel > normalFade + 0.1f) //Incase it has been pulsing or is a strong shield
        {
            FadingOut();
        }
        else
            fading = false;
    }

    private void FadingOut()
    {
        if (fadeLevel > 0)
        {
            //Debug.Log(fadeLevel);
            fadeLevel -= fadeSpeed * Time.deltaTime;
            prop.SetFloat("_Visibility", fadeLevel);
            rend.SetPropertyBlock(prop);
        }
        else
            fading = false;
    }

    //The shield fades in and out fast.
    private void Pulsing()
    {
        fadeLevel = Mathf.PingPong(Time.time + 0.2f, 0.5f); //Pulses with an interval of two seconds.
        prop.SetFloat("_Visibility", fadeLevel);
        rend.SetPropertyBlock(prop);
    }

    //The shield looks much brighter
    private void StrongShield()
    {
        if (fadeLevel < maxFade)
        {
            fadeLevel += fadeSpeed * Time.deltaTime;
            prop.SetFloat("_Visibility", fadeLevel);
            rend.SetPropertyBlock(prop);
        }
        else
            fading = true;
    }


    private void UseFunction(Shield input)
    {
        switch (input)
        {
            case Shield.FadeIn:
                FadingIn();
                break;
            case Shield.FadeOut:
                FadingOut();
                break;
            case Shield.Pulse:
                Pulsing();
                break;
            case Shield.Strong:
                StrongShield();
                break;
            default:
                break;
        }
    }

}
