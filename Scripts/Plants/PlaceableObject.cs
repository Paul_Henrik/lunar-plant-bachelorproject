﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//Used for collisin detection when planting or placing objects.
public class PlaceableObject : MonoBehaviour {

    public List<Collider> colliders = new List<Collider>();

    public Type objectType;
    public enum Type
    {
        Plant,
        Object
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Plant") || other.CompareTag("Unplantable"))
        {
            colliders.Add(other);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if(other.CompareTag("Plant") || other.CompareTag("Unplantable"))
        {
            colliders.Remove(other);
        }
    }
}
