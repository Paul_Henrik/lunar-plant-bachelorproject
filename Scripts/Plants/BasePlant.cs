﻿using System.Collections;
using System;
using UnityEngine;
using UnityEngine.Assertions;


public class BasePlant : MonoBehaviour
{
    #region Variables
    //SerializeField is not inheritated, therefor they are public so they are shown in the inspector.
    //Waterbased Variables
    [Range(0, 5)]
    public float waterUsage;    //how much water the plant uses over time.
    public bool watered;        //to water the plant
    protected float waterLevel; //How much water the plant currently have. Set to full when watered.

    //Timebased Variables
    public Vector3 plantGrowthTime; //How long time the plant needs to grow up.
    public Vector3[] stages;    //When the plant is changing stage
    public GameObject[] stageModels;    //The models for the different stages.
    public Vector3 startSize;   //The size the plant will start in
    public float growthRate;    //Feedback to the player. The plant grows over time.
    protected Clock universalClock; //The current worldtime
    protected bool paused;  //Pauses the growth of the plant.
    protected Vector3 pausedAt; //Keeps track of when it was paused.
    protected Vector3 plantTime;    //Dosent really do anything, but could be used for info of the plant.
    protected Vector3 plantFinishedAt;  //When the plant is finnished. Will be changed if the growth is paused.
    protected Vector3 finnishWithBonus; //The time the plant is finnished at if it have a bonus
    protected Vector3 plantMaxSize; //Make the plant grow to the proper size. Not to big, not to small <3
    protected bool hasLight = false;

    //other
    public State plantState;    //What state the plant is in
    public int dropSeedAmount;  //How many seeds the plant will drop
    public bool fullgrown = false;  //If the plant is fullgrown
    protected bool planted; //So it won't start growing before the player has planted the plant.
    protected bool harvested = false;   //Checks if the plant has been harvested or not.
    public float radius;    //Radius for grass, shield, aoe effect.
    public float radiusSpeed;
    private float maxRadius;    //How big the radius will get.
    protected Seed.Type plantType;  //What type of plant it is
    protected GameObject shieldPlant;   //Assign a shieldplant if the plant have one.
    protected Renderer rend;
    protected bool died = false;    //If the plant have died
    protected int currentStage;   //What stage the plant currently is at
    public PlantData data;
    public Animator anim;
    #endregion

    public enum State   //Gets the state of the plant. Can be used to indicate how state of the plant or if it needs anything.
    {
        Growing,
        Harvestable,
        Dehydrated,
        Mutating,
        Dead
    }

    protected virtual void Awake()
    {
        data = new PlantData();
        plantMaxSize = transform.localScale;    //Records the max size for the plant
        plantType = GetComponent<Seed>().seedType;  //Records the seed type
        universalClock = GameObject.FindGameObjectWithTag("Manager").GetComponent<Clock>(); //Gets the universal time
        rend = transform.GetChild(0).GetComponent<SkinnedMeshRenderer>();
        maxRadius = radius;
    }

    protected virtual void Update()
    {
        if (planted)
        {
            if (!universalClock.pauseTime)  //checks that the game is not paused.
            {

                if (!fullgrown && !died)    //Checks if the plant is still growing
                {
                    Watered();  //checks the plant condition according to waterlevel.
                    Growing();  //Makes the plant grow
                }
                if (fullgrown && !died)   //If the plant is fullgrown 
                {
                    FullGrown();    //For the derived classes
                }
                Special();

            }
        }
    }

    #region Functions

    /// <summary>
    /// Activates the plant and makes it start growing
    /// </summary>
    public virtual void Planted()
    {
        if(anim != null)
        anim.enabled = true;
        plantTime = universalClock.getTime();   //Records what time the plant was planted.
        plantFinishedAt = plantTime + plantGrowthTime;  //Sets the time the plant will be finnished.
        plantFinishedAt = Clock.ReformatToTime(plantFinishedAt);    //Reformat the vector to a time format.
        for (int i = 0; i < stages.Length; i++)
        {
            stages[i] += plantTime;
            stages[i] = Clock.ReformatToTime(stages[i]);
        }
        //transform.localScale = startSize;   //makes the plant small
        currentStage = 0;   //sets the stage to null
        harvested = false;  //makes sure the plant is not harvestable
        planted = true;     //Allows the growth and water functions to beging
        fullgrown = false;
        waterLevel = 70;    //Sets the water level so the plant does not instadies.
        GetComponent<HighlightObject>().SetMaterial(HighlightObject.Type.Normal);   //sets the material of the plant to normal.
        radius = 0;
    }


    protected virtual void Growing()
    {
        Vector3 uc = universalClock.getTime();  //Gets the current ingame time (plantFinishedAt.x <= uc.x) && (plantFinishedAt.y <= uc.y) && (plantFinishedAt.z <= uc.z)
        if (!paused)    //So the plant won't change stage or be fullgrown if the growth is paused. 
        {
            finnishWithBonus = (hasLight) ? plantFinishedAt * 0.95f : plantFinishedAt;  //Finnishes faster if the plant have a lightplant next to it.
            if ((currentStage < stages.Length) && VectorIsSmaller(stages[currentStage], uc))    //Changes the state of the plant
            {
                if (currentStage == 0)
                    rend.gameObject.SetActive(false);
                else
                    stageModels[currentStage - 1].SetActive(false);
                currentStage++;
                rend = stageModels[currentStage - 1].transform.GetChild(0).GetComponent<SkinnedMeshRenderer>();
                stageModels[currentStage - 1].SetActive(true);
                GetComponent<HighlightObject>().SetRenderer(rend);
                GetComponent<FlowerStateDisplay>().NewPos();
            }
            else if (VectorIsSmaller(finnishWithBonus, uc)) //checks if the plant is finnished.
            {
                fullgrown = true;
            }
        }
    }

    /// <summary>
    /// Makes the plant grow depending on the water level.
    /// </summary>
    protected virtual void Watered()
    {
        if (waterLevel > 40)
        {
            if (VectorIsSmaller(transform.localScale, plantMaxSize)) //Makes sure the plant grow over its max size
            {

                if (radius < maxRadius)
                    radius += radiusSpeed * Time.deltaTime;
            }
            plantState = State.Growing;
            if (paused) //Unpauses the growth and set the plants new finnish time.
            {
                plantFinishedAt = plantFinishedAt + AbsoluteValueVector(universalClock.getTime() - pausedAt);   //Gets the absolute value since the vector can be minus
                plantFinishedAt = Clock.ReformatToTime(plantFinishedAt);
                for (int i = 0; i < stages.Length; i++)
                {
                    stages[i] = stages[i] + AbsoluteValueVector(universalClock.getTime() - pausedAt);
                    stages[i] = Clock.ReformatToTime(stages[i]);
                }
                paused = false;
            }
        }
        else
        {
            if (!paused)    //Stops the plants growth until its watered again.
            {
                plantState = State.Dehydrated;  //Feedback to player
                paused = true;
                anim.SetTrigger("Wither");
                pausedAt = universalClock.getTime();
            }
            else if (waterLevel < 10)   //Kills the plant
            {
                plantState = State.Dead;    //Feedback to player
                died = true;    //turns of main mechanics.
                killPlant(2);
            }
        }
        waterLevel -= Time.deltaTime * waterUsage;  //Reduces the waterlevel
    }

    /// <summary>
    /// Kills the plant on x amount of time.
    /// </summary>
    /// <param name="timeToKillPlant"></param>
    public virtual void killPlant(float timeToKillPlant)    //Kills a plant and removes it in x seconds
    {
        FindObjectOfType<MainManager>().GetComponent<Grass>().plants.Remove(gameObject.transform);
        StartCoroutine(PlantDying(timeToKillPlant));
    }

    /// <summary>
    /// The plant spend X amount on dying.
    /// </summary>
    /// <param name="waitTime"></param>
    /// <returns></returns>
    public virtual IEnumerator PlantDying(float waitTime)   //plants turn gray, tile can be replanted and plant is removed
    {
        died = true;
        yield return new WaitForSeconds(waitTime);

        Destroy(gameObject);
    }


    /// <summary>
    /// Harvest the plant, yields two seeds and then kills the plant on the set time. 
    /// </summary>
    /// <param name="killPlantTime"></param>
    public virtual void HarvestPlant(float killPlantTime)
    {
        if (fullgrown)
        {
            GetComponent<Sellables>().SpawnSellablesLoot(1);
        }

        if (!harvested)
        {
            GetComponent<Seed>().SpawnSeedLoot(plantType, dropSeedAmount, fullgrown);
            killPlant(killPlantTime);
            harvested = true;   //So it can't be harvested several times.
        }
    }

    /// <summary>
    /// Assign the plant a shieldplant.
    /// </summary>
    /// <param name="_shieldPlant"></param>
    public void SetShieldPlant(GameObject _shieldPlant)
    {
        shieldPlant = _shieldPlant;
    }

    /// <summary>
    /// Checks if a vector is smaller.
    /// </summary>
    /// <param name="smaller"></param>
    /// <param name="bigger"></param>
    /// <returns></returns>
    public static bool VectorIsSmaller(Vector3 smaller, Vector3 bigger)
    {
        if ((smaller.x <= bigger.x) && (smaller.y <= bigger.y) && (smaller.z <= bigger.z))
            return true;
        else
            return false;
    }

    /// <summary>
    /// Returns a vector with posetive values.
    /// </summary>
    /// <param name="absThis"></param>
    /// <returns></returns>
    public static Vector3 AbsoluteValueVector(Vector3 absThis)
    {
        Vector3 absVector;
        absVector.x = Mathf.Abs(absThis.x);
        absVector.y = Mathf.Abs(absThis.y);
        absVector.z = Mathf.Abs(absThis.z);

        return absVector;
    }

    /// <summary>
    /// Water the plant
    /// </summary>
    public void WaterPlant()
    {
        StartCoroutine("EndOfFrame");
        waterLevel = 100;
        anim.SetTrigger("Shake");
        plantState = State.Growing;
    }

    /// <summary>
    /// What happens when the plant is fullgrown.
    /// </summary>
    protected virtual void FullGrown()
    {
        plantState = State.Harvestable; //For feedback system.
    }

    private IEnumerator EndOfFrame()
    {
        watered = true;
        yield return 0;
        watered = false;
    }

    /// <summary>
    /// For derived classes, uses their special abilities every frame.
    /// </summary>
    protected virtual void Special()
    {

    }
    #endregion

    #region Get/Set
    public State GetPlantState()
    {
        return plantState;
    }

    public float GetWaterLevel()
    {
        return waterLevel;
    }

    public bool GetPlanted()
    {
        return planted;
    }

    public bool GetHarvested()
    {
        return harvested;
    }


    public Seed.Type GetPlantType()
    {
        return plantType;
    }

    public void SetLight(bool LightBonus)
    {
        hasLight = LightBonus;
    }
    #endregion


    #region SaveLoad

    void OnEnable()
    {
        SaveLoadManager.OnLoaded += LoadData;
        SaveLoadManager.OnBeforeSave += StoreData;
        SaveLoadManager.OnBeforeSave += ApplyData;
    }

    void OnDisable()
    {
        SaveLoadManager.OnLoaded -= LoadData;
        SaveLoadManager.OnBeforeSave -= StoreData;
        SaveLoadManager.OnBeforeSave -= ApplyData;
    }

    public void StoreData()
    {
        data._position = transform.position;
        data._rotation = transform.rotation;
        data._scale = transform.localScale;
        data._plantType = (int)plantType;
        data._waterLevel = waterLevel;
        data._state = (int)plantState;
        data._fullgrown = fullgrown;
        data._plantFinishedAt = plantFinishedAt;
        data._currentStage = currentStage;
    }

    public void LoadData()
    {
        transform.localScale = data._scale;
        waterLevel = data._waterLevel;
        plantState = State.Growing + data._state;
        fullgrown = data._fullgrown;
        plantFinishedAt = data._plantFinishedAt;
        currentStage = data._currentStage;

        if (anim != null)
            anim.enabled = true;
        planted = true;
        for (int i = 0; i < stages.Length; i++)
        {
            stages[i] += universalClock.getTime();
            stages[i] = Clock.ReformatToTime(stages[i]);
        }
    }

    public void ApplyData()
    {
        SaveLoadManager.AddData(data);
    }

    public Renderer GetRenderer()
    {
        return rend;
    }

    #endregion
}

[Serializable]
public class PlantData
{
    public Vector3 _position;

    public Quaternion _rotation;

    public Vector3 _scale;

    public int _plantType;

    public float _waterLevel;

    public int _state;

    public bool _fullgrown;

    public Vector3 _plantFinishedAt;

    public int _currentStage;
}