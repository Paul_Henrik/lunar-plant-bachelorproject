﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterPlant : BasePlant
{
	public LayerMask mask;
	public List<GameObject> plantsInRadius;
	[SerializeField]private Collider plantCollider;  //The collider which isn't a trigger
	public bool waterSurroundings;  //
	public float waterTimer;
	private float waterTimerReset;

	new void Awake()
	{
		base.Awake();
		plantCollider.enabled = false;
		waterTimerReset = waterTimer;
	}

	public override void Planted()
	{
		base.Planted();
		plantCollider.enabled = true;
	}

	private void AddPlantsToList()
	{
		Collider[] protectedPlants = Physics.OverlapSphere(transform.position, radius, mask);
		if (protectedPlants.Length != plantsInRadius.Count)
		{
			plantsInRadius.Clear();
			foreach (Collider col in protectedPlants)
			{
                if (!col.CompareTag("Plant"))
                    continue;
				Vector3 distance = col.transform.position - transform.position;
				if (distance.magnitude < radius + 1)
				{
					plantsInRadius.Add(col.gameObject);
				}
			}
		}
	}

	public void WaterSurroundingPlants()
	{
		bool reset = false;
		foreach (GameObject item in plantsInRadius)
		{
			if (item == null)
			{
				plantsInRadius.Remove(item);
				reset = true;
				break;
			}
			item.GetComponent<BasePlant>().WaterPlant();
		}
		if (reset)
			WaterSurroundingPlants();
	}

	protected override void FullGrown()
	{
		if (plantState != State.Harvestable)
			plantState = State.Harvestable;
		rend.material.color = Color.cyan;
		AddPlantsToList();

		waterTimer -= Time.deltaTime;
		waterSurroundings = (waterTimer < 0) ? true : false;
		if (waterSurroundings)
		{
			waterTimer = waterTimerReset;
			WaterSurroundingPlants();
			waterSurroundings = false;
		}
	}

}
