﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Seed : MonoBehaviour
{
#pragma warning disable 0414    //Removed not used warning because it is being used.
    private GameObject seedObject;
#pragma warning restore 0414
    public Type seedType;   //This objects seedtype.
    private GameObject[] seedPrefab;

    public enum Type
    {
        Normal,
        Shield,
        Water,
        Light,
        Fruit,
        Crocus,
        None
    }

    //Loads the prefabs from resources. 
    void Awake()
    {
        seedPrefab = new GameObject[6];
        seedPrefab[0] = (GameObject)Resources.Load("NormalSeed");
        seedPrefab[1] = (GameObject)Resources.Load("ShieldSeed");
        seedPrefab[2] = (GameObject)Resources.Load("WaterSeed");
        seedPrefab[3] = (GameObject)Resources.Load("LightSeed");
        seedPrefab[4] = (GameObject)Resources.Load("FruitSeed");
        seedPrefab[5] = (GameObject)Resources.Load("CrocusSeed");
    }

    /// <summary>
    /// Spawns seed loot and amount of seeds.
    /// </summary>
    /// <param name="seedType"></param>
    /// <param name="amount"></param>
    public void SpawnSeedLoot(Seed.Type seedType, int amount, bool fullGrown) //If fullgrown, full drop. If not, only 1 seed drop
    {
        switch (seedType)
        {
            case Type.Normal:
                if (fullGrown)
                {
                    for (int i = 0; i < amount; i++)
                    {
                        seedObject = Instantiate(seedPrefab[(int)Type.Normal], transform.position, Quaternion.identity);
                    }
                }
                else
                {
                    seedObject = Instantiate(seedPrefab[(int)Type.Normal], transform.position, Quaternion.identity);
                }
                break;
            case Type.Shield:
                if (fullGrown)
                {
                    for (int i = 0; i < amount; i++)
                    {
                        seedObject = Instantiate(seedPrefab[(int)Type.Shield], transform.position, Quaternion.identity);
                    }
                }
                else
                {
                    seedObject = Instantiate(seedPrefab[(int)Type.Shield], transform.position, Quaternion.identity);
                }
                break;
            case Type.Water:
                if (fullGrown)
                {
                    for (int i = 0; i < amount; i++)
                    {
                        seedObject = Instantiate(seedPrefab[(int)Type.Water], transform.position, Quaternion.identity);
                    }
                }
                else
                {
                    seedObject = Instantiate(seedPrefab[(int)Type.Water], transform.position, Quaternion.identity);
                }
                break;
            case Type.Light:
                if (fullGrown)
                {
                    for (int i = 0; i < amount; i++)
                    {
                        seedObject = Instantiate(seedPrefab[(int)Type.Light], transform.position, Quaternion.identity);
                    }
                }
                else
                {
                    seedObject = Instantiate(seedPrefab[(int)Type.Light], transform.position, Quaternion.identity);
                }
                break;
            case Type.Fruit:
                if (fullGrown)
                {
                    for (int i = 0; i < amount; i++)
                    {
                        seedObject = Instantiate(seedPrefab[(int)Type.Fruit], transform.position, Quaternion.identity);
                    }
                }
                else
                {
                    seedObject = Instantiate(seedPrefab[(int)Type.Fruit], transform.position, Quaternion.identity);
                }
                break;
            case Type.Crocus:
                if (fullGrown)
                {
                    for (int i = 0; i < amount; i++)
                    {
                        seedObject = Instantiate(seedPrefab[(int)Type.Crocus], transform.position, Quaternion.identity);
                    }
                }
                else
                {
                    seedObject = Instantiate(seedPrefab[(int)Type.Crocus], transform.position, Quaternion.identity);
                }
                break;
            case Type.None:
                return;
            default:
                break;
        }
    }

    /// <summary>
    /// Returns the plant type
    /// </summary>
    /// <returns></returns>
    public Seed.Type GetSeedType()
    {
        return seedType;
    }

    /// <summary>
    /// Moves to player
    /// </summary>
    /// <param name="playerPos"></param>
    public void AttractToPlayer(Vector3 playerPos)
    {
        Vector3 attractDir = playerPos - transform.position;

        transform.position += attractDir * Time.deltaTime;
    }
}
