﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyboxScript : MonoBehaviour {
    [SerializeField]
    public float exposure;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        RenderSettings.skybox.SetFloat("_Exposure", exposure);
	}
}
