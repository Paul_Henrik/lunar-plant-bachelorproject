﻿using UnityEngine;

public class GravityAttractor : MonoBehaviour
{
    public float gravityScale = -6f; // Used to determine how much force pulls an object to the asteroid. Lower than gravity (-9.81) to simulate low gravity
    
    public void Attract(Transform body, float weight, bool rotateObject)
    {
        // Pulls the body towards the center of the asteroid. Also makes sure to place and rotate the body object correctly in regards to
        // the curvature of the asteroid.
        Vector3 targetDir = (body.position - transform.position).normalized; // Vector pointing towards center of asteroid
        Vector3 bodyUpDir = body.up;

        if(rotateObject)
        {
            body.rotation = Quaternion.FromToRotation(bodyUpDir, targetDir) * body.rotation; // Makes the bodyUpDir vector line up with the targetDir. Add this rotation to the current rotation of body
        }
        body.gameObject.GetComponent<Rigidbody>().AddForce(targetDir * gravityScale * weight); // Add force to pull the object towards the center of the asteroid.
    }
}
