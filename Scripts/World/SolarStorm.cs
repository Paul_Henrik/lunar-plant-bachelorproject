﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class SolarStorm : MonoBehaviour
{

    public float exposure;  //the current intensity and color of sun/sky
    private float exposureTime; //How long the sky and ground will light up
    private bool SolarStormInvoked = false; //used during a solarstorm. 
    private Clock universalTime;    //Current time
    private Vector3 nextSolarStorm; //When the next solarstorm is happening. 
    private Vector3 nextWarningLight;
    private Vector3 nextCriticalLight;
    public Vector3 solarStormInterval; //How frequent the solarstorms are happening.
    public Light sun;   //Used to light up the ground
    private Color sunNormalColor;   //The suns normal color
    private Color lerpedSunColor; //The current color inbetween lerps
    public Color sunSolarStormColor;    //The color that will be lerped too
    public int amountOfSolarStorms;     //Amount of solarstorms that have happend.
    public GameObject warningLight; //Warning light on the clock when the storm approaches
    public GameObject criticalLight; //Warning light on the clock when the storm is right around the corner
    [SerializeField]
    private Vector3[] solarStormIntervals;
    public AudioSource[] stormSound = new AudioSource[2];


    void Start()
    {
        Assert.IsNotNull(sun, "Direction light have not beeen activated in: " + gameObject);
        Assert.IsNotNull(warningLight, "The warning light from the clock is missing in: " + gameObject);
        Assert.IsNotNull(criticalLight, "The critical light from the clock is missing in: " + gameObject);
        solarStormInterval = solarStormIntervals[0];
        universalTime = GetComponent<Clock>();
        nextSolarStorm = universalTime.getTime() + solarStormInterval;
        sunNormalColor = sun.color;
        nextWarningLight = Clock.ReformatToTime(universalTime.getTime() + (solarStormInterval - new Vector3(0, 4, 0)));
        nextCriticalLight = Clock.ReformatToTime(universalTime.getTime() + (solarStormInterval - new Vector3(0, 1, 0)));
        //Debug.Log(nextWarningLight);  //Check if the warning lights are correct
        //Debug.Log(nextCriticalLight);
    }
    // Update is called once per frame
    void Update()
    {
        //If a 3rd of the time is left, warning light
        if(BasePlant.VectorIsSmaller(nextWarningLight, universalTime.getTime()) && !warningLight.activeSelf)
        {
            warningLight.SetActive(true);
        }
        //If 1 hour is left, critical light
        else if(BasePlant.VectorIsSmaller(nextCriticalLight, universalTime.getTime()) && !criticalLight.activeSelf)
        {
            warningLight.SetActive(false);
            criticalLight.SetActive(true);
        }

        if (SolarStormInvoked)
        {
            SkyboxExposure();
        }
        if (BasePlant.VectorIsSmaller(nextSolarStorm, universalTime.getTime()))
        {
            SolarStormInvoked = true;
            StartCoroutine(StartMutating(1f));

            if (universalTime.getTime().z % 3 == 0)      //Random interval on Solarstorms.
            {
                int setInterval = Random.Range(0, 2);
                Debug.Log(setInterval);
                ChangeInterval(setInterval);
            }
            nextSolarStorm = Clock.ReformatToTime(universalTime.getTime() + solarStormInterval);
            nextWarningLight = Clock.ReformatToTime(universalTime.getTime() + (solarStormInterval - new Vector3(0, 4, 0)));
            nextCriticalLight = Clock.ReformatToTime(universalTime.getTime() + (solarStormInterval - new Vector3(0, 1, 0)));
            
            //Debug.Log(nextWarningLight);  //Check if the warninglights are correct
            //Debug.Log(nextCriticalLight);


            warningLight.SetActive(false);
            criticalLight.SetActive(false);
        }

        //if (Input.GetKeyDown(KeyCode.O))    //Used for debugging
        //{
        //    SolarStormInvoked = true;
        //    StartCoroutine(StartMutating(1f));
        //}
    }

    /// <summary>
    /// Starts the mutation part of a solarstorm.
    /// </summary>
    public void InvokeSolarStorm()  //Makes a solarstorm affect all the plants
    {
        amountOfSolarStorms++;
        //SolarStormInvoked = true;
        Mutate[] targets = FindObjectsOfType(typeof(Mutate)) as Mutate[];
        foreach (Mutate plant in targets)
        {
            plant.TryToMutate();
        }

        int newTrack = Random.Range(0, 2); //Pick randomly between 2 sounds
        stormSound[newTrack].Play(); //Plays an explotiony sound when a storm is invoked
    }

    public void SkyboxExposure()
    {
        if (exposureTime > 3)
        {
            exposureTime = 0;
            RenderSettings.skybox.SetFloat("_Exposure", 1);
            exposure = 1;
            sun.intensity = 1;
            sun.color = sunNormalColor;
            lerpedSunColor = sunNormalColor;

            SolarStormInvoked = false;
        }
        else
        {
            //Debug.Log(exposureTime);
            lerpedSunColor = Color.Lerp(sunSolarStormColor, sunNormalColor, Mathf.PingPong(Time.time, 2.5f));
            sun.color = lerpedSunColor;
            exposure = -1.9f * Mathf.Pow(exposureTime, 2) + 5.55f * exposureTime + 1f;
            if (exposure > 1)
            {
                sun.intensity = exposure / 1.5f;
                RenderSettings.skybox.SetFloat("_Exposure", exposure);

            }
            exposureTime += Time.deltaTime;
        }
    }

    public void ChangeInterval(int intervalSpeed)
    {
        solarStormInterval = solarStormIntervals[intervalSpeed];
    }

    private IEnumerator StartMutating(float time)
    {
        yield return new WaitForSeconds(time);
        InvokeSolarStorm();
    }

    /// <summary>
    /// Check if a solarstorm is currently happening.
    /// </summary>
    /// <returns></returns>
    public bool GetSolarStorm()
    {
        return SolarStormInvoked;
    }
}
