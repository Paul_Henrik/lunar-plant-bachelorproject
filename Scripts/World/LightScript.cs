﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightScript : MonoBehaviour 
{
    private new Light light;
    public float speed;
    public float maxIntensity;
    public float minIntensity;
    private bool increase;

	void Start()
	{
        light = GetComponent<Light>();
        increase = true;
        light.intensity = Random.Range(minIntensity, maxIntensity);
	}
 
	void Update() 
	{
        light.intensity = (increase) ? Increase() : Decrease();
	}

    private float Increase()
    {
        if (maxIntensity <= light.intensity)
            increase = false;
        return light.intensity += speed * Time.deltaTime;
    }

    private float Decrease()
    {
        if (light.intensity < minIntensity)
            increase = true;
        return light.intensity -= speed * Time.deltaTime;
    }
 
}
