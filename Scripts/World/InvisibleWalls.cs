﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InvisibleWalls : MonoBehaviour 
{
    [Range(0,90)] public float newDegree;
    private float oldDegree;
    PlayerMovement pm;

    private void Start()
    {
        pm = FindObjectOfType<PlayerMovement>();
        oldDegree = pm.GetAngle();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
        {
            pm = other.GetComponent<PlayerMovement>();
            pm.SetAngle(newDegree);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            pm.SetAngle(oldDegree);
        }
    }
}
