﻿using System.Collections.Generic;
using System;

[Serializable]
public class DataContainers //Holds all data that needs to be saved!
{
    public List <InventoryData> _inventoryData = new List<InventoryData>();
    public List <SettingsData> _settingsData = new List<SettingsData>();
    public List <PlantData> _plantData = new List<PlantData>();
    public List <WorldData> _worldData = new List<WorldData>();
	
}
