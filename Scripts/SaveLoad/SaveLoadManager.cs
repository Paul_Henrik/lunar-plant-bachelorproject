﻿using UnityEngine;
using System.IO;

public class SaveLoadManager : MonoBehaviour
{
    public static DataContainers containers = new DataContainers(); //All the data that needs to be saved
    public static bool loadScene = false;

    //Serializes information when needed
    public delegate void SerializeAction();
    public static event SerializeAction OnLoaded;
    public static event SerializeAction OnBeforeSave;

    /// <summary>
    /// Loads all data
    /// </summary>
    /// <param name="path">Where the file to load is located</param>
    public static void Load(string path)
    {
        MainManager mainManager = GameObject.FindGameObjectWithTag("Manager").GetComponent<MainManager>();
        containers = LoadContainers(path);
        mainManager.ApplyOnetimeData(containers);
        
        foreach(PlantData plant in containers._plantData)
        {
            int tempType = plant._plantType;
            Vector3 tempPos = plant._position;
            Quaternion tempRot = plant._rotation;

            mainManager.InstantiatePlantPrefab(Seed.Type.Normal + tempType, tempPos, tempRot, plant);

        }

        OnLoaded();
        ClearAllData();
    }

    /// <summary>
    /// Saves all data
    /// </summary>
    /// <param name="path"> Where to save the file</param>
    /// <param name="data"> What data to save</param>
    public static void Save(string path, DataContainers data)
    {
        OnBeforeSave();

        SaveAllData(path, data);

        ClearAllData();
    }
    
    public static void ClearAllData()
    {
        containers._inventoryData.Clear();
        containers._settingsData.Clear();
        containers._plantData.Clear();
        containers._worldData.Clear();
    }

    #region AddDataOverrides 
    //One override for each type of data saved
    public static void AddData(InventoryData data)
    {
        containers._inventoryData.Add(data);
    }

    public static void AddData(SettingsData data)
    {
        containers._settingsData.Add(data);
    }
    
    public static void AddData(PlantData data)
    {
        containers._plantData.Add(data);
    }
    
    public static void AddData(WorldData data)
    {
        containers._worldData.Add(data);
    }
    #endregion

    private static DataContainers LoadContainers(string path) //Loads the data into the containers again
    {
        string json = File.ReadAllText(path); //Reads the json file

        return JsonUtility.FromJson<DataContainers>(json); //Translates that text into actual data that can be used in game
    }

    private static void SaveAllData(string path, DataContainers data) //Takes all the data and writes it into the save file
    {
        string json = JsonUtility.ToJson(data);

        StreamWriter sw = File.CreateText(path);
        sw.Close();

        File.WriteAllText(path, json);
    }
	
}
