﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Destroys the particle engines in the system under. Used in Particle Aura prefab. 

public class DestroyParticleEngine : MonoBehaviour 
{
    private ParticleSystem ps;

    void Start()
    {
        ps = GetComponentInChildren<ParticleSystem>();
    }
    void Update() 
	{
        if (!ps.IsAlive())
            Destroy(gameObject);
	}
 
}
