﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Tutorial from Craig Perko
//https://www.youtube.com/watch?reload=9&v=wYAlky1aZn4
//Optimization.
//Combines meshes to reduce batches.

public class MeshCombiner : MonoBehaviour 
{
    //Used from a button in Inspector. See MeshCombinerEditor script. 
    public void CombineMeshes()
    {
        Quaternion oldRot = transform.rotation;
        Vector3 oldPos = transform.position;

        transform.rotation = Quaternion.identity;
        transform.position = Vector3.zero;

        MeshFilter[] filters = GetComponentsInChildren<MeshFilter>();

        Debug.Log(name + " is combining " + filters.Length + " meshes!" );

        Mesh finalMesh = new Mesh();    //The mesh that will be generated.
        finalMesh.name = "Generated Mesh";

        CombineInstance[] combiners = new CombineInstance[filters.Length];  //The meshes that will be combined.

        for (int i = 0; i < filters.Length; i++)
        {
            if (filters[i].transform == transform)
                continue;

            combiners[i].subMeshIndex = 0;
            combiners[i].mesh = filters[i].sharedMesh;
            combiners[i].transform = filters[i].transform.localToWorldMatrix;
        }

        finalMesh.RecalculateBounds();
        finalMesh.CombineMeshes(combiners); //Finnish combinding meshes.

        GetComponent<MeshFilter>().sharedMesh = finalMesh;

        transform.rotation = oldRot;
        transform.position = oldPos;

        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }
    }
}
