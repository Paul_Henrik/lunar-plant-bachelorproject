﻿using UnityEngine;

//Not implemented due to time. 

public class Sellables : MonoBehaviour
{
    [SerializeField]
    private string itemName; 
    [Range(0, 1000), SerializeField] private int buyPrice;
    [Range(0, 1000), SerializeField] private int sellPrice;
    private int amount = 0;
    public Type type;
    public GameObject sellablesObject;
    private GameObject[] sellablesPrefab = new GameObject[6];


    public enum Shop    //Choose a enum since I think the code looks more readable with it.
    {
        Buy,
        Sell
    }

    public enum Type
    {
        Fruit,
        CommonHead,
        ShieldHead,
        WaterHead,
        LightHead,
        CrocusHead,
        None
    }

    private void Start()
    {
        if(GetComponent<Seed>())
        {
            itemName = GetComponent<Seed>().GetSeedType().ToString();
        }

        sellablesPrefab[0] = (GameObject)Resources.Load("Fruit");
        sellablesPrefab[1] = (GameObject)Resources.Load("Common_Head");
        sellablesPrefab[2] = (GameObject)Resources.Load("Shield_Head");
        sellablesPrefab[3] = (GameObject)Resources.Load("Water_Head");
        sellablesPrefab[4] = (GameObject)Resources.Load("Light_Head");
        sellablesPrefab[5] = (GameObject)Resources.Load("Crocus_Head");
    }

    public void SetPrice(int newPrice ,Shop shop)
    {
        switch (shop)
        {
            case Shop.Buy:
                buyPrice = newPrice;
                break;
            case Shop.Sell:
                sellPrice = newPrice;
                break;
            default:
                break;
        }
    }

    public int GetPrice(Shop price)
    {
        switch (price)
        {
            case Shop.Buy:
                return buyPrice;
            case Shop.Sell:
                return sellPrice;
            default:
                Debug.LogWarning("Something wong, you broke script", gameObject);
                return -9000;    //Must have something here, easy to catch a bug if something happens.  
        }
    }

    public void SetName(string newName)
    {
        itemName = newName;
    }

    public string GetName()
    {
        return itemName;
    }

    public void SetSellableType(Type tempType)
    {
        type = tempType;
    }

    public Type GetSellableType()
    {
        return type;
    }

    public void AttractToPlayer(Vector3 playerPos)
    {
        Vector3 attractDir = playerPos - transform.position;

        transform.position += attractDir * Time.deltaTime;
    }

    public void SpawnSellablesLoot(int amount)
    {
        if(GetComponent<Seed>().GetSeedType() != Seed.Type.Fruit)
        for(int i = 0; i < amount; i++)
            sellablesObject = Instantiate(sellablesPrefab[(int)type], transform.position, Quaternion.identity);
    }

    #region Amount Functions
    public  void AddAmount(int add)
    {
        amount += add;
    }

    public void SubAmount(int subtract)
    {
        amount -= subtract;
    }

    public int GetAmount()
    {
        return amount;
    }

    public void SetAmount(int setAmount)
    {
        amount = setAmount;
    }
    #endregion
}
