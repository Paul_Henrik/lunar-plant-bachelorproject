﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Makes the texture on the spaceship look more alive.

public class ShieldScript : MonoBehaviour {

    Material moveShield;
    Renderer rend;

    void Start()
    {
        rend = GetComponent<Renderer>();
    }
    void Update()   
    {
        //Moves the texture according to Time. 
        float ScaleY = Mathf.Sin(Time.time) * 0.5f + 1;
        rend.material.mainTextureScale = new Vector2(rend.material.mainTextureScale.x, ScaleY );
    }
}
