﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;

public class HighlightObject : MonoBehaviour
{
	private Type currentMaterial;    //The current material the object is used choosen in inspector.
    [SerializeField]
    private Material highlight;  //The material used when the object is highlighted.
    [SerializeField]
    private Material notAvailable;   //The material used when the object is selected.
    [SerializeField]
    private Material watered;    //The material used when the object is watered.
    [SerializeField]
    private Material hologramRed;    //Red hologram for placement
    [SerializeField]
    private Material hologramGreen;  //Green hologram material for placement
	private Material normal;    //The common material of the object.
	private Material[] normalMaterials; //If the objects have several common materials.
	private bool hologram;      //If the hologram is on or off.
	private Material selectedMaterial;  //Material selected through the enum
    [SerializeField]
    private Renderer rend;  //Get the main renderer. 
	private MeshRenderer[] childRenders;    //Get all the transforms children

	//This script might need for loops to get all the materials assigned to a object
	//when objects gets textured.
	public enum Type
	{
		Normal,
		NotAvailable,
		Highlight,
		Watered
	}
	// Use this for initialization
	void Awake()
	{
        if (gameObject.CompareTag("Plant"))
            rend = transform.GetChild(0).GetComponent<SkinnedMeshRenderer>();
        else if (gameObject.CompareTag("Unplantable"))
            rend = transform.GetChild(0).GetComponent<MeshRenderer>();  //Quickfix workaround for fruit. 
        else
            rend = GetComponent<MeshRenderer>();

		normal = gameObject.CompareTag("Plant") || gameObject.CompareTag("Unplantable") ? rend.material : GetComponent<MeshRenderer>().material;    //Check if this is needed. seems to be fixed above.

        //Alerts the users if the objects don't have a material.
        Assert.IsNotNull(highlight, "Highlight material is not set in: " + this.name);
		Assert.IsNotNull(notAvailable, "Selected material is not set in: " + this.name);
		Assert.IsNotNull(hologramRed, "Hologram Red not set in: " + this.name);
		Assert.IsNotNull(hologramGreen, "Hologram Green not set in: " + this.name);

		childRenders = GetComponentsInChildren<MeshRenderer>();

		if (rend.materials.Length > 1)
		{
			normalMaterials = new Material[rend.materials.Length];
			for (int i = 0; i < rend.materials.Length; i++)
			{
				normalMaterials[i] = rend.materials[i];
			}
		}

	}

    /// <summary>
    /// Set what the material the plant should have.
    /// </summary>
    /// <param name="type"></param>
	public void SetMaterial(Type type)
	{
		if (currentMaterial == type)
			return;
		switch (type)   //Set chosen material
		{
			case Type.Normal:
				selectedMaterial = normal;
				currentMaterial = Type.Normal;
				break;
			case Type.NotAvailable:
				selectedMaterial = (!hologram) ? notAvailable : hologramRed;
				currentMaterial = Type.NotAvailable;
				break;
			case Type.Highlight:
				selectedMaterial = (!hologram) ? highlight : hologramGreen;
				currentMaterial = Type.Highlight;
				break;
			case Type.Watered:
				selectedMaterial = watered;
				currentMaterial = Type.Watered;
				break;
			default:
				break;
		}
		rend.material = selectedMaterial;

        //Set the childrens materials.
		if (childRenders.Length > 0)
		{
			foreach (Renderer renderer in childRenders)
			{
				if (renderer.gameObject.layer == LayerMask.NameToLayer("Interactable"))
					renderer.material = rend.material;
			}
		}
		if (rend.materials.Length > 1)
		{
			if (type != Type.Normal)
			{
				Material[] mats = rend.materials;
				for (int i = 0; i < rend.materials.Length; i++)
				{
					mats[i] = selectedMaterial;
				}
				rend.materials = mats;
			}
			else
			{
				rend.materials = normalMaterials;
			}

		}
	}

    /// <summary>
    /// Get the plants status.
    /// </summary>
    /// <returns></returns>
	public Type GetStatus()
	{
		return currentMaterial;
	}

    /// <summary>
    /// Sets the materiald color.
    /// </summary>
    /// <param name="inColor"></param>
	public void SetNormalMaterialColor(Color inColor)
	{
		normal.color = inColor;
	}

	/// <summary>
	/// Changes the materials Highlight and NotAvailable to Holograms
	/// <para>True turns on the hologram, false turns of</para>
	/// </summary>
	/// <param name="On"></param>
	public void SetHologram(bool hologramMode)
	{
		hologram = hologramMode;     //Sets the hologram mode on and off
	}

    /// <summary>
    /// Changes the renderer
    /// </summary>
    /// <param name="input"></param>
    public void SetRenderer(Renderer input)
    {
        rend = input;
        normal = input.material;
    }
}
