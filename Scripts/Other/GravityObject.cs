﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))] //The project will not run if this gameObject does not have a rigidbody

public class GravityObject : MonoBehaviour
{
    GravityAttractor asteroid; //Object of the gravity attractor script. Used to get access to the Attract function
    Rigidbody rBody; //The objects rigidbody. Used to restrict the automatic physics this applies.
    public float weight; //How much the gravity will affect the object
    public bool rotateObject; //Only rotates the object if this bool calls for it
    
    void Awake()
    {
        asteroid = GameObject.FindGameObjectWithTag("Asteroid").GetComponent<GravityAttractor>(); //The main asteroid
        rBody = GetComponent<Rigidbody>();
        rBody.useGravity = false;
        rBody.constraints = RigidbodyConstraints.FreezeRotation;
    }
    
    void FixedUpdate()
    {
        //Attracts this object to the asteroid.
        asteroid.Attract(transform, weight, rotateObject);
    }
}
