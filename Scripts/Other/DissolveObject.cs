﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DissolveObject : MonoBehaviour
{
    [SerializeField]
    private Material dissolve;      //The material with dissolve shader.
    private Material[] materials;   //The materials the objects have.
    [Range(0, 1)]
    public float dissolveLevel; //how dissolved the object is. 0 is whole. on 1 the whole object is gone.
    [Range(0, 1)]
    public float speed; //How fast the dissolve will happen.
    public bool fading; //If the object us currently fading.
    public bool fadeOut;    //If the object is fading in or out.
    private Mesh mesh;  //Reference
    private Renderer rend;  //Reference
    private MaterialPropertyBlock prop; //To only affect this objects material.
    private PlaceableObject.Type objectType;    //Not implemented, used to remove objects instead of plants.

    void Start()
    {
        rend = transform.GetChild(0).GetComponent<Renderer>();
        if (GetComponent<MeshFilter>() != null)
            mesh = GetComponent<MeshFilter>().mesh;
        prop = new MaterialPropertyBlock();
        objectType = GetComponent<PlaceableObject>().objectType;
        ObjectType(objectType);
    }

    void Update()
    {
        ObjectType(objectType); //Updates every frame. Need a better way to do this
        if (fading) //Starts fading the object
        {
            if (null != gameObject.GetComponent<HighlightObject>())
            {
                Destroy(GetComponent<HighlightObject>());   //Player can't affect highlight anymore.
                gameObject.layer = 0;   //Is set to a non interactable object (Default)
                gameObject.tag = "Untagged";    //Untags so it dosen't get noticed in a RayCast. Because I destroy Highlight Object.    
            }

            if (!fadeOut)   //Fades in an object.
                FadeInObject();
            else
                FadeOutObject();    //Fades out an object
            Graphics.DrawMesh(mesh, Vector3.zero, Quaternion.identity, dissolve, 0); //Upgrades the material
        }
    }

    /// <summary>
    /// Fades in the objects
    /// </summary>
    public void FadeInObject()  //Fades in the object
    {
        rend = GetComponent<BasePlant>().GetRenderer(); //Changes the objects different stages.
        if (dissolveLevel > 1)
            dissolveLevel = 1;
        dissolveLevel += speed * Time.deltaTime;
        prop.SetFloat("_SliceAmount", dissolveLevel);
        rend.SetPropertyBlock(prop);
        rend.material = dissolve;
        DissolveChildren();
    }

    /// <summary>
    /// Fades out the object
    /// </summary>
    public void FadeOutObject()
    {
        rend = GetComponent<BasePlant>().GetRenderer();
        if (dissolveLevel < 0)
            dissolveLevel = 0;
        dissolveLevel -= speed * Time.deltaTime;
        prop.SetFloat("_SliceAmount", dissolveLevel);
        rend.SetPropertyBlock(prop);
        rend.material = dissolve;
        DissolveChildren();
    }

    //This was made so the dissolve could be used on rocks, junk etc.
    //The player could be able to remove rocks for money. 
    private void ObjectType(PlaceableObject.Type item)
    {
        switch (item)
        {
            case PlaceableObject.Type.Plant:
                fading = GetComponent<BasePlant>().GetHarvested();
                break;
            case PlaceableObject.Type.Object:
                break;
            default:
                break;
        }

    }

    /// <summary>
    /// Uses dissolve on child meshes.
    /// </summary>
    private void DissolveChildren()
    {
        rend = GetComponent<BasePlant>().GetRenderer();
        if (rend.materials.Length > 1)
        {
            Material[] mats = rend.materials;
            for (int i = 0; i < rend.materials.Length; i++)
            {
                mats[i] = dissolve;
            }
            rend.materials = mats;
        }
        //DissolveChildMeshes();
    }

    /// <summary>
    /// Dissolve childmeshes as well.
    /// </summary>
    private void DissolveChildMeshes()
    {
        if (transform.childCount <= 0)
            return;

        Renderer[] mats = GetComponentsInChildren<Renderer>();
        foreach (var item in mats)
        {
            item.material = dissolve;
        }
    }
}
