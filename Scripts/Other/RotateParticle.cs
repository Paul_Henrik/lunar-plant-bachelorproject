﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Since the instantiated particles always starts in a weird angle. 

public class RotateParticle : MonoBehaviour
{
	void Start()
	{
		transform.Rotate(new Vector3(-90, 0, 0));
	}
}
