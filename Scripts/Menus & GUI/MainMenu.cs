﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public AudioSource buttonHover; //Sound effect for hovering over button
    public AudioSource buttonClicked; //Sound effect for clicking button
    public AudioSource sliderTick; //Sound effect for sliders (ticking)

    public GameObject loadingImage; //Loading text in the corner
    private Animator[] animators; //Used to trigger the dance

    private string saveLoadPath; //The path the file will be saved at
    private bool sceneActivate = false; //Waits to activate the main scene until the player presses new game or load game

    void Awake()
    {
        saveLoadPath = System.IO.Path.Combine(Application.persistentDataPath, "Save1.json");
        Debug.Log(saveLoadPath); //Make sure the path is correct
        animators = FindObjectsOfType<Animator>(); 
    }

    void Start()
    {
        Cursor.visible = true;
        StartCoroutine(LoadAsync());
    }

    /// <summary>
    /// When the player presses the new game button, this function will trigger. 
    /// It runs a sound, then loads the mainscene as it is set up by default.
    /// </summary>
    public void NewGame()
    {
        ButtonClick();
        sceneActivate = true;

        foreach (Animator anim in animators)
        {
            anim.SetBool("Loading", true);
        }

        loadingImage.SetActive(true);
    }

    /// <summary>
    /// This function runs when the load game button is clicked.
    /// It will run the scene as per normal, but it will load some information from a document saved previously
    /// </summary>
    public void LoadGame()
    {
        ButtonClick();
        sceneActivate = true;
        SaveLoadManager.loadScene = true;

        foreach (Animator anim in animators)
        {
            anim.SetBool("Loading", true);
        }

        loadingImage.SetActive(true);
    }

    /// <summary>
    /// This function runs when the exit game button is pressed.
    /// It runs a sound, then exits (closes) the application.
    /// </summary>
    public void ExitGame()
    {
        ButtonClick();
        Application.Quit();
    }

    /// <summary>
    /// Loads the level, then waits for the player to activate it
    /// </summary>
    /// <returns></returns>
    IEnumerator LoadAsync()
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(SceneManager.GetActiveScene().buildIndex + 1);

        operation.allowSceneActivation = false;

        while(true)
        {
            if(sceneActivate)
            {
                operation.allowSceneActivation = true;
                break;
            }

            yield return null;
        }
    }


    #region Sound effect functions
    /// <summary>
    /// Sound playing when the mouse hovers over a button in the main menu
    /// </summary>
    public void ButtonHover()
    {
        buttonHover.Play();
    }

    /// <summary>
    /// Sound playing when a button in the main menu is clicked
    /// </summary>
    public void ButtonClick()
    {
        buttonClicked.Play();
    }

    /// <summary>
    /// Sound playing (ticking) when a slider is adjusted
    /// </summary>
    public void SliderTick()
    {
        sliderTick.Play();
    }
    #endregion
}
