﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial : MonoBehaviour
{
    public GameObject[] tutPanels = new GameObject[9]; //0 = intro. 1-6 beginner tuts. 7 - Marketplace. 8 - Crocus.
    private bool showCursor = false; //If a panel is open, the cursor should be visible on screen.
    private int currentPanelOpen = 1; //Which small panel is open? Will update when a new one is opened
    public MainManager mainManager; //Referance to the main manager for functions and variables
    public PlayerInventory playerInventory; //Used to check if the player has picked up a crocus

    //Triggers for later tutorial popups!
    public bool crocusFound = false;
    public bool marketShown = false;

    bool showTips = true; // If false, the game will stop showing tips on screen. The directions in the settings menu will still be available

	void Start ()
    {
        mainManager = GameObject.FindGameObjectWithTag("Manager").GetComponent<MainManager>();
        playerInventory = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerInventory>();

		if (showTips && tutPanels[0].activeSelf) //Open the introduction screen on start
        {
            IntroductionScreen();
        }
	}

    void Update()
    {
        if(playerInventory.inventorySeed.ContainsKey(Seed.Type.Crocus) && !crocusFound) //Crocus tut pops up if this is the first seed picked up
        {
            crocusFound = true;
            tutPanels[8].SetActive(true);
        }

        if(mainManager.GetComponent<Clock>().getTime().y == 12 && !marketShown) //Shows marketplace after 12 ingame hours of gameplay
        {
            marketShown = true;
            tutPanels[7].SetActive(true);
        }
    }

    /// <summary>
    /// Returns showTips bool
    /// </summary>
    /// <returns></returns>
    public bool GetShowTips()
    {
        return showTips;
    }

    /// <summary>
    /// Changes the showTips bool
    /// </summary>
    /// <param name="tips"></param>
    public void SetShowTips(bool tips)
    {
        showTips = tips;
    }

    /// <summary>
    /// Moves one up in the panel array
    /// </summary>
    public void ButtonRight()
    {
        int i = currentPanelOpen + 1;
        if (i == 9)
            i = 1; //Loops the array if it's all the way on one end

        UpdatePanel(i);
    }
    
    /// <summary>
    /// Moves one down in the panel array
    /// </summary>
    public void ButtonLeft()
    {
        int i = currentPanelOpen - 1;
        if (i == 0)
            i = 8; //Loops the array if it's all the way on one end

        UpdatePanel(i);
    }

    /// <summary>
    /// Updates the visible panel based on input value. Sets that panel active
    /// </summary>
    /// <param name="newPanel"></param>
    private void UpdatePanel(int newPanel)
    {
        tutPanels[currentPanelOpen].SetActive(false);
        tutPanels[newPanel].SetActive(true);
        currentPanelOpen = newPanel;
    }


    #region Introduction

    /// <summary>
    /// Sets some variables for if the introduction screen is open
    /// </summary>
    public void IntroductionScreen()
    {
        mainManager.overlayOpen = true;
        Cursor.visible = true;
        mainManager.FreezeCam();
    }

    /// <summary>
    /// When the introduction panel is closed, this function resets the variables set in the IntroductionScreen function
    /// </summary>
    public void IntroductionClose()
    {
        mainManager.UnfreezeCam();
        UpdatePanel(currentPanelOpen);
    }

    #endregion
}
