﻿using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SettingsMenu : MonoBehaviour
{
    public AudioMixer mainAudioMixer; //The game's audiomixer. Used to set volume

    Resolution[] resolutions; //List of all resolutions available for your machine
    public Dropdown resolutionDropdown; // The dropdown menu for resolutions
    public Dropdown graphicDropdown; // The dropdown menu for the graphic settings
    public Dropdown monitorDropdown; //Dropdown to choose monitor if there are multiple accessable monitors
    public Toggle fullscreenToggle; // The UI element to toggle fullscreen
    public Toggle vsyncToggle; //The UI element to toggle vsync
    public Slider masterSlider; //The slider for the master volume
    public Slider sfxSlider; //The slider for the sfx volume
    public Slider musicSlider; //The slider for the music volume
    public Slider sensYSlider; //The slider for the sensitivity on the y axis for the camera
    public Slider sensXSlider; //The slider for the sensitivity on the x axis for the camera
    MainManager mainManager; //Referance to the main manager

    public GameObject displayMenu; //The overall display menu
    public GameObject cameraMenu; //The overall camera menu
    public GameObject graphicsMenu; //The overall graphics menu
    public GameObject audioMenu; //The overall audio menu
    public GameObject inputMenu; //The overall input menu (renamed "controls")

    public bool mainMenu; //If current scene is mainMenu, the script won't look for mainManager
    public SettingsData data; //For storing and saving data

    GameObject activeMenu; //The currently active menu

    void Awake()
    {
        data = new SettingsData();
        resolutionDropdown.ClearOptions();
        if(monitorDropdown)
        monitorDropdown.ClearOptions();

        resolutions = Screen.resolutions;

        //Get all the possible resolutions for your machine and put them in a list
        //Input these options into the dropdown menu. Make sure the active one is the one showing
        List<string> resolutionText = new List<string>();
        List<string> displayText = new List<string>();
        int currentResIndex = 0;
        int currentQualityIndex = QualitySettings.GetQualityLevel();
        int currentDisplayIndex = 0;

        for (int i = 0; i < resolutions.Length; i++)
        {
            string temp = resolutions[i].width + " x " + resolutions[i].height;
            resolutionText.Add(temp);

            if (resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height)
            {
                currentResIndex = i;
            }
        }

        displayText.Add("Default");

        if (Display.displays.Length > 1)
        {
            for (int i = 1; i < Display.displays.Length; i++)
            {
                string temp =(i + 1).ToString();
                displayText.Add(temp);
            }
        }

        //Add options to the display/monitor dropdown and set which monitor is active
        if(monitorDropdown)
        {
            monitorDropdown.AddOptions(displayText);
            monitorDropdown.value = currentDisplayIndex;
            monitorDropdown.RefreshShownValue();
        }
        
        //Add options to the resolution dropdown and set which one is active
        resolutionDropdown.AddOptions(resolutionText);
        resolutionDropdown.value = currentResIndex;
        resolutionDropdown.RefreshShownValue();

        //Make sure the active graphics settings is the one showing
        graphicDropdown.value = currentQualityIndex;
        graphicDropdown.RefreshShownValue();

        //Always start the settings menu on display
        activeMenu = displayMenu;

        if(!mainMenu)
            mainManager = GameObject.FindGameObjectWithTag("Manager").GetComponent<MainManager>();
    }

    #region MenuSwitchButtons

    public void displayButton()
    {
        if (activeMenu != displayMenu)
        {
            activeMenu.SetActive(false);
            activeMenu = displayMenu;
            activeMenu.SetActive(true);
        }
    }

    public void cameraButton()
    {
        if (activeMenu != cameraMenu)
        {
            activeMenu.SetActive(false);
            activeMenu = cameraMenu;
            activeMenu.SetActive(true);
        }
    }

    public void graphicsButton()
    {
        if (activeMenu != graphicsMenu)
        {
            activeMenu.SetActive(false);
            activeMenu = graphicsMenu;
            activeMenu.SetActive(true);
        }
    }

    public void audioButton()
    {
        if (activeMenu != audioMenu)
        {
            activeMenu.SetActive(false);
            activeMenu = audioMenu;
            activeMenu.SetActive(true);
        }
    }

    public void inputButton()
    {
        if (activeMenu != inputMenu)
        {
            activeMenu.SetActive(false);
            activeMenu = inputMenu;
            activeMenu.SetActive(true);
        }
    }

    #endregion

    #region displayMenu

    public void SetFullscreen(bool fullscreen)
    {
        Screen.fullScreen = fullscreen;
    }

    public void SetVsync(bool sync)
    {
        if (!sync)
            QualitySettings.vSyncCount = 0;
        else
            QualitySettings.vSyncCount = 1;
    }

    public void SetResolution(int resIndex)
    {
        Screen.SetResolution(resolutions[resIndex].width, resolutions[resIndex].height, Screen.fullScreen);
    }

    public void SetBrightness (float brightness)
    {
        RenderSettings.ambientLight = new Color(brightness, brightness, brightness, 1f);
    }

    #endregion

    #region cameraMenu
    //Changes camera sensitivities
    public void SensXChange(float newSens)
    {
        mainManager.SetSensX(newSens);
        mainManager.UnfreezeCam();
    }

    public void SensYChange(float newSens)
    {
        mainManager.SetSensY(newSens);
        mainManager.UnfreezeCam();
    }

    #endregion

    #region graphicsMenu

    public void SetGraphicQuality(int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
    }

    #endregion

    #region audioMenu

    public void SetMasterVolume(float volume)
    {
        mainAudioMixer.SetFloat("masterVolume", volume);
    }

    public void SetFXVolume(float volume)
    {
        mainAudioMixer.SetFloat("fxVolume", volume);
    }

    public void SetMusicVolume(float volume)
    {
        mainAudioMixer.SetFloat("musicVolume", volume);
    }

    #endregion

    #region SaveLoad

    void OnEnable()
    {
        SaveLoadManager.OnLoaded -= LoadData;
        SaveLoadManager.OnBeforeSave -= StoreData;
        SaveLoadManager.OnBeforeSave -= ApplyData;
        SaveLoadManager.OnLoaded += LoadData;
        SaveLoadManager.OnBeforeSave += StoreData;
        SaveLoadManager.OnBeforeSave += ApplyData;
    }

    public void StoreData()
    {
        data._resIndex = resolutionDropdown.value;
        data._graphicIndex = graphicDropdown.value;
        data._fullscreen = Screen.fullScreen;
        data._vsync = (QualitySettings.vSyncCount == 1) ? true : false;
        data._sensY = mainManager.GetSensY();
        data._sensX = mainManager.GetSensX();
        mainAudioMixer.GetFloat("masterVolume", out data._masterVolume);
        mainAudioMixer.GetFloat("fxVolume", out data._sfxVolume);
        mainAudioMixer.GetFloat("musicVolume", out data._musicVolume); 
    }

    public void LoadData()
    {
        SetResolution(data._resIndex);
        resolutionDropdown.value = data._resIndex;
        resolutionDropdown.RefreshShownValue();
        
        SetGraphicQuality(data._graphicIndex);
        graphicDropdown.value = data._graphicIndex;
        graphicDropdown.RefreshShownValue();

        SetFullscreen(data._fullscreen);
        fullscreenToggle.isOn = data._fullscreen;

        SetVsync(data._vsync);
        vsyncToggle.isOn = data._vsync;

        mainManager.SetSensX(data._sensX);
        sensXSlider.value = data._sensX;

        mainManager.SetSensY(data._sensY);
        sensYSlider.value = data._sensY;

        mainAudioMixer.SetFloat("masterVolume", data._masterVolume);
        masterSlider.value = data._masterVolume;

        mainAudioMixer.SetFloat("fxVolume", data._sfxVolume);
        sfxSlider.value = data._sfxVolume;

        mainAudioMixer.SetFloat("musicVolume", data._musicVolume);
        musicSlider.value = data._musicVolume;

    }

    public void ApplyData()
    {
        SaveLoadManager.AddData(data);
    }

    #endregion


}

[Serializable]
public class SettingsData
{
    public int _resIndex;
    public int _graphicIndex;
    public bool _fullscreen;
    public bool _vsync;
    public float _sensY;
    public float _sensX;
    public float _masterVolume;
    public float _sfxVolume;
    public float _musicVolume;
}