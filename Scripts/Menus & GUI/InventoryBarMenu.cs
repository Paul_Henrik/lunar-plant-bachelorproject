﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;

public class InventoryBarMenu : MonoBehaviour
{
    //List of slots in the inventory bar
    public List<InventoryPetal> slots = new List<InventoryPetal>();

    int highlightIndex = 0; //Used to determine which slot is active
    int numberOfDifferentPlants = 0; //Records the number of different plants currently in the inventory, as only seeds are visible in the bar menu
    [SerializeField]
    private PlayerInventory inventory; //Reference to the inventory to display correctly
    [SerializeField]
    private FullInventory fullInventory; //Object for the full inventory that displays all items currently in inventory
    [SerializeField]
    private GameObject highlight; //The highlight object that indicates to the player which slot is active

    void Awake()
    {
        inventory = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerInventory>();

        InventoryPetal[] petalsTemp = GetComponentsInChildren<InventoryPetal>();
        for (int i = 0; i < petalsTemp.Length; i++)
        {
            slots.Add(petalsTemp[i]);
        }

        //Sets the tools as the 2 first slots by default. These slots are bound and cannot be moved
        slots[0].type = InventoryPetal.PetalType.Barmenu;
        slots[0].tool = PlayerInventory.Tool.Watercan;
        slots[0].seed = Seed.Type.None;
        slots[0].UpdateSprite();

        slots[1].type = InventoryPetal.PetalType.Barmenu;
        slots[1].tool = PlayerInventory.Tool.Shovel;
        slots[1].seed = Seed.Type.None;
        slots[1].UpdateSprite();
    }

    void Start()
    {
        RedoMenu(); //Sets up the menu from the beginning
        numberOfDifferentPlants = inventory.inventorySeed.Count;
    }

    void Update()
    {

        if (Input.GetAxis("Mouse ScrollWheel") > 0) //Scroll up on the wheel will move the highlight down
        {
            highlightIndex--;
            if (highlightIndex < 0)
            {
                highlightIndex = 8;
            }
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0) //Scroll down on the wheel will move the highlight up
        {
            highlightIndex++;
            if (highlightIndex > 8)
            {
                highlightIndex = 0;
            }
        }
        //Using number buttons to set a spesific slot
        else if (Input.GetKeyDown("1"))
        {
            highlightIndex = 0;
        }
        else if (Input.GetKeyDown("2"))
        {
            highlightIndex = 1;
        }
        else if (Input.GetKeyDown("3"))
        {
            highlightIndex = 2;
        }
        else if (Input.GetKeyDown("4"))
        {
            highlightIndex = 3;
        }
        else if (Input.GetKeyDown("5"))
        {
            highlightIndex = 4;
        }
        else if (Input.GetKeyDown("6"))
        {
            highlightIndex = 5;
        }
        else if (Input.GetKeyDown("7"))
        {
            highlightIndex = 6;
        }
        else if (Input.GetKeyDown("8"))
        {
            highlightIndex = 7;
        }
        else if (Input.GetKeyDown("9"))
        {
            highlightIndex = 8;
        }

        //Places highlight correctly
        highlight.transform.position = slots[highlightIndex].transform.position;

        //If a new plant is added to the inventory, or a plant is removed fully, the inventory is redone to reflect this
        if (numberOfDifferentPlants != inventory.inventorySeed.Count)
        {
            RedoMenu();
            numberOfDifferentPlants = inventory.inventorySeed.Count;
        }
        
        for (int i = 2; i < 9; i++) //Updates the number on the slots each frame. Not optimal
        {
            int v;
            if (slots[i].seed != Seed.Type.None)
            {
                if (inventory.inventorySeed.TryGetValue(slots[i].seed, out v))
                {
                    slots[i].objectAmount = v;
                }
            }

            else if (slots[i].sellableType != Sellables.Type.None)
            {
                if (inventory.inventorySellables.TryGetValue(slots[i].sellableType, out v))
                {
                    slots[i].objectAmount = v;
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.Tab) && Time.timeScale != 0) //Opens and closes full inventory
        {
            if (fullInventory.gameObject.activeSelf)
            {
                fullInventory.gameObject.SetActive(false);
            }
            else
                fullInventory.gameObject.SetActive(true);
        }

        ChangeValues();
    }

    /// <summary>
    /// Sets up the menu from scratch. Clears all slots, then fill them again based on the current information
    /// </summary>
    void RedoMenu()
    {
        for (int i = 2; i < slots.Count; i++)
        {
            slots[i].ClearPetal();
        }

        int t;

        for (int i = 0; i < 6; i++)
        {
            if (inventory.inventorySeed.TryGetValue(Seed.Type.Normal + i, out t))
            {
                slots[i + 2].seed = Seed.Type.Normal + i;
                slots[i + 2].objectAmount = t;
            }
        }

        foreach (InventoryPetal petal in slots)
        {
            petal.UpdateSprite();
        }
    }

    /// <summary>
    /// Sets the actual values in the inventory so they can be used correctly
    /// </summary>
    void ChangeValues()
    {
        inventory.SetTool(slots[highlightIndex].tool); //If tool is not equipped, will be set to free (for planting)
        inventory.SetSelectedSeed(slots[highlightIndex].seed); //If tool is selected, will be set to none
    }
}
