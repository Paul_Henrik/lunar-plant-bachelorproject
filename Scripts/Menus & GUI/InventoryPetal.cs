﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryPetal : MonoBehaviour
{
    public Seed.Type seed = Seed.Type.None; //The petal can hold a seed
    public int objectAmount;
    public Sellables.Type sellableType = Sellables.Type.None;
    public PlayerInventory.Tool tool = PlayerInventory.Tool.Free;
    public int upgradeType = -1;

    //Sprite icons seeds
    [SerializeField]
    private Sprite commonPlantSeed;
    [SerializeField]
    private Sprite shieldPlantSeed;
    [SerializeField]
    private Sprite waterPlantSeed;
    [SerializeField]
    private Sprite lightPlantSeed;
    [SerializeField]
    private Sprite fruitPlantSeed;
    [SerializeField]
    private Sprite crocusSeed;

    //Sprite icons flowerheads
    [SerializeField]
    private Sprite commonPlant;
    [SerializeField]
    private Sprite shieldPlant;
    [SerializeField]
    private Sprite waterPlant;
    [SerializeField]
    private Sprite lightPlant;
    [SerializeField]
    private Sprite fruitPlant;
    [SerializeField]
    private Sprite crocusPlant;

    //Sprite icons objects
    [SerializeField]
    private Sprite fruit;

    //Sprite icons for upgrades
    [SerializeField]
    private Sprite waterLvl1;
    [SerializeField]
    private Sprite waterLvl2;
    [SerializeField]
    private Sprite waterLvl3;
    [SerializeField]
    private Sprite harvestLvl1;
    [SerializeField]
    private Sprite harvestLvl2;
    [SerializeField]
    private Sprite harvestLvl3;
    [SerializeField]
    private Sprite bow;

    //Sprite icons tools
    [SerializeField]
    private Sprite wateringCan;
    [SerializeField]
    private Sprite harvestShovel;


    //Displaying information on the petal
    [SerializeField]
    private Image icon;
    [SerializeField]
    private Text infoText;

    public string title; //The petal title, based on what item it holds
    public PetalType type; //The petal type, based on what the petal is used for
    public int value = 0; //The value of the item, set for marketplace, to be able to buy and sell

    //Sound effects for hover and clicking a petal
    private AudioSource hoverSound;
    private AudioSource clickSound;

    public enum PetalType //Enum that sets the type of petal
    {
        Barmenu,
        FullInventoryMenu,
        MarketmenuSell,
        MarketmenuBuy
    }

    void Awake()
    {
        ClearPetal(); //Clears the petal when instanciated
    }

    void Start()
    {
        hoverSound = GameObject.Find("ButtonHover").GetComponent<AudioSource>();
        clickSound = GameObject.Find("ButtonClick").GetComponent<AudioSource>();
    }

    void Update()
    {
        if (seed != Seed.Type.None && type != PetalType.MarketmenuBuy) //If the petal is in the buy menu, it displays name rather than number
            infoText.text = objectAmount.ToString();
    }

    /// <summary>
    /// Clear and reset all the information on the petal
    /// </summary>
    public void ClearPetal()
    {
        seed = Seed.Type.None;
        sellableType = Sellables.Type.None;
        upgradeType = -1;
        tool = PlayerInventory.Tool.Free;
        objectAmount = 0;
        icon.sprite = null;
        infoText.text = "";

        Color tempColor = icon.color;
        tempColor.a = 0f;
        icon.color = tempColor;
    }

    /// <summary>
    /// Updates the sprite and text displayed on a petal, so it matches the actual information held by the petal
    /// </summary>
    public void UpdateSprite()
    {
        if (tool != PlayerInventory.Tool.Free)
        {
            seed = Seed.Type.None;
            switch (tool)
            {
                case PlayerInventory.Tool.Watercan:
                    icon.sprite = wateringCan;
                    infoText.text = "Water";
                    title = "Watercan";
                    break;
                case PlayerInventory.Tool.Shovel:
                    icon.sprite = harvestShovel;
                    infoText.text = "Harvest";
                    title = "Harvest tool";
                    break;
                default:
                    break;
            }
            Color tempColor = icon.color;
            tempColor.a = 1f;
            icon.color = tempColor;
        }
        else if (seed != Seed.Type.None)
        {
            switch (seed)
            {
                case Seed.Type.Light:
                    icon.sprite = lightPlantSeed;
                    infoText.text = "Light";
                    title = "Light plant";
                    value = 60;
                    break;
                case Seed.Type.Normal:
                    icon.sprite = commonPlantSeed;
                    infoText.text = "Common";
                    title = "Common plant";
                    value = 5;
                    break;
                case Seed.Type.Shield:
                    icon.sprite = shieldPlantSeed;
                    infoText.text = "Shield";
                    title = "Shield plant";
                    value = 50;
                    break;
                case Seed.Type.Water:
                    icon.sprite = waterPlantSeed;
                    infoText.text = "Water";
                    title = "Water plant";
                    value = 75;
                    break;
                case Seed.Type.Fruit:
                    icon.sprite = fruitPlantSeed;
                    infoText.text = "Fruit";
                    title = "Fruit plant";
                    value = 50;
                    break;
                case Seed.Type.Crocus:
                    icon.sprite = crocusSeed;
                    infoText.text = "Crocus";
                    title = "Crocus plant";
                    value = 200;
                    break;
                default:
                    break;
            }
            Color tempColor = icon.color;
            tempColor.a = 1f;
            icon.color = tempColor;
        }
        else if (sellableType != Sellables.Type.None)
        {
            switch (sellableType)
            {
                case Sellables.Type.Fruit:
                    icon.sprite = fruit;
                    infoText.text = "Fruit";
                    title = "Fruit";
                    value = 300;
                    break;
                case Sellables.Type.CommonHead:
                    icon.sprite = commonPlant;
                    infoText.text = "Common";
                    title = "Commonplant head";
                    value = 15;
                    break;
                case Sellables.Type.ShieldHead:
                    icon.sprite = shieldPlant;
                    infoText.text = "Shield";
                    title = "Shieldplant Head";
                    value = 75;
                    break;
                case Sellables.Type.WaterHead:
                    icon.sprite = waterPlant;
                    infoText.text = "Water";
                    title = "Waterplant Head";
                    value = 150;
                    break;
                case Sellables.Type.LightHead:
                    icon.sprite = lightPlant;
                    infoText.text = "Light";
                    title = "Lightplant Head";
                    value = 100;
                    break;
                case Sellables.Type.CrocusHead:
                    icon.sprite = crocusPlant;
                    infoText.text = "Crocus";
                    title = "Crocus Head";
                    value = 600;
                    break;
                default:
                    break;
            }
            Color tempColor = icon.color;
            tempColor.a = 1f;
            icon.color = tempColor;
        }
        else if (upgradeType != -1)
        {
            switch (upgradeType)
            {
                case 0:
                    icon.sprite = harvestLvl1;
                    infoText.text = "Harvest";
                    title = "Harvest level 1";
                    value = 1000;
                    break;

                case 1:
                    icon.sprite = harvestLvl2;
                    infoText.text = "Harvest";
                    title = "Harvest level 2";
                    value = 1500;
                    break;

                case 2:
                    icon.sprite = harvestLvl3;
                    infoText.text = "Harvest";
                    title = "Harvest level 3";
                    value = 2000;
                    break;

                case 3:
                    icon.sprite = waterLvl1;
                    infoText.text = "Watering";
                    title = "Watering level 1";
                    value = 1000;
                    break;

                case 4:
                    icon.sprite = waterLvl2;
                    infoText.text = "Watering";
                    title = "Watering level 2";
                    value = 1500;
                    break;

                case 5:
                    icon.sprite = waterLvl3;
                    infoText.text = "Watering";
                    title = "Watering level 3";
                    value = 2000;
                    break;

                case 6:
                    icon.sprite = bow;
                    infoText.text = "Bow";
                    title = "Bow, head";
                    value = 5000;
                    break;

                case 7:
                    icon.sprite = bow;
                    infoText.text = "Bow";
                    title = "Bow, neck";
                    value = 5000;
                    break;

                default:
                    break;
            }
            //Turns up opacity on the picture when it is set
            Color tempColor = icon.color;
            tempColor.a = 1f;
            icon.color = tempColor;
        }
            if (type != PetalType.MarketmenuBuy && tool == PlayerInventory.Tool.Free) 
                infoText.text = objectAmount.ToString();
    }

    /// <summary>
    /// The petal grows slightly when mouse is over it
    /// </summary>
    public void MouseEnter()
    {
        transform.localScale = new Vector3(1.2f, 1.2f, 1.2f);
        if( type == PetalType.FullInventoryMenu)
        {
            GameObject.Find("FullInventoryMenu").GetComponent<FullInventory>().SetActiveSquare(this);
        }
        hoverSound.Play();
    }

    /// <summary>
    /// The petal size is reset when the mouse is no longer over it
    /// </summary>
    public void MouseExit()
    {
        transform.localScale = Vector3.one;
        if (type == PetalType.FullInventoryMenu)
        {
            GameObject.Find("FullInventoryMenu").GetComponent<FullInventory>().SetActiveSquare(null);
        }
    }

    /// <summary>
    /// When a petal is clicked in the marketplace menu
    /// </summary>
    public void MarketMouseClick()
    {
        GameObject.Find("MarketplaceMenu").GetComponent<MarketPlace>().UpdateInformationCenter(this);
        clickSound.Play();
    }
}
