﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MarketPlace : MonoBehaviour
{
    public GameObject buyMenuGrid; //The grid for the buyable objects 
    public GameObject sellMenuGrid; //The grid for the sellable objects 
    public InventoryPetal squarePrefab; //The prefab for each square
    private List<InventoryPetal> buyItemList = new List<InventoryPetal>(); //List of items currently on the buy side
    private List<InventoryPetal> sellItemList = new List<InventoryPetal>(); //List of items currently on the sell side
    public PlayerInventory inventory; //Referance for the player's inventory for both buy and sell functions
    public MainManager mainManager; // To freeze and unfreeze cam
    public Text renorkText; //Text to show currency
    private InventoryPetal currentPetal;

    //GameObjects for center
    public Text objectTitle; //Title of the object currently selected
    public Text objectDescription; //A short description of the currently selected object
    public Button buySellButton; //The button at the bottom of the center, deciding to buy or sell
    public Sprite buyText;
    public Sprite sellText;

    //Sound
    public AudioSource petalHover;
    public AudioSource petalClick;
    public AudioSource itemBought;
    public AudioSource notEnoughMoney;

    void Awake()
    {
        mainManager = GameObject.FindGameObjectWithTag("Manager").GetComponent<MainManager>();
    }

    /// <summary>
    /// Sets up the buy menu from the start, so it's not completely empty
    /// </summary>
    void Start()
    {
        BuyMenu(); //Set up the buy menu (has the same stuff each time)
        SellMenu(); //Set up the sell menu (based on the player inventory)
    }

    /// <summary>
    /// Update the money text each frame
    /// </summary>
    void Update()
    {
        renorkText.text = ": " + inventory.renorks.ToString(); //Update the money text
    }

    /// <summary>
    /// Empties the current item list. Then starts filling the menu with items that the player is able to buy
    /// These objects will have a name under them rather than a number, since the market has an unlimited number of them
    /// This menu is set up manually, so each buyable item needs to be added here by hand
    /// </summary>
    public void BuyMenu()
    {
        foreach (InventoryPetal item in buyItemList)
        {
            if (item != currentPetal || item.upgradeType != -1)
                Destroy(item.gameObject);
        }
        buyItemList.Clear();

        for (int i = 0; i < 5; i++) //Add seeds to the buy list, this is temporary
        {
            if (mainManager.discoveredPlants[i])
            {
                GameObject temp = Instantiate(squarePrefab.gameObject);
                temp.transform.SetParent(buyMenuGrid.transform);
                temp.transform.localScale = Vector3.one;
                temp.GetComponent<InventoryPetal>().seed = Seed.Type.Normal + i;
                temp.GetComponent<InventoryPetal>().type = InventoryPetal.PetalType.MarketmenuBuy;
                temp.GetComponent<InventoryPetal>().UpdateSprite();
                buyItemList.Add(temp.GetComponent<InventoryPetal>());
            }
        }

        bool[] tempUpgrades = inventory.GetComponent<PlayerUpgrades>().allUpgrade;

        for(int i = 0; i < tempUpgrades.Length; i++)
        {
            if(!tempUpgrades[i] && i < 3) //Harvest
            {
                GameObject temp = Instantiate(squarePrefab.gameObject);
                temp.transform.SetParent(buyMenuGrid.transform);
                temp.transform.localScale = Vector3.one;
                temp.GetComponent<InventoryPetal>().type = InventoryPetal.PetalType.MarketmenuBuy;
                temp.GetComponent<InventoryPetal>().upgradeType = i;
                temp.GetComponent<InventoryPetal>().UpdateSprite();
                buyItemList.Add(temp.GetComponent<InventoryPetal>());
                i = 2;
            }
            else if (!tempUpgrades[i] && i < 6) //Water
            {
                GameObject temp = Instantiate(squarePrefab.gameObject);
                temp.transform.SetParent(buyMenuGrid.transform);
                temp.transform.localScale = Vector3.one;
                temp.GetComponent<InventoryPetal>().type = InventoryPetal.PetalType.MarketmenuBuy;
                temp.GetComponent<InventoryPetal>().upgradeType = i;
                temp.GetComponent<InventoryPetal>().UpdateSprite();
                buyItemList.Add(temp.GetComponent<InventoryPetal>());
                i = 5;
            }
            else if (!tempUpgrades[6] && i == 6) // bow head
            {
                GameObject temp = Instantiate(squarePrefab.gameObject);
                temp.transform.SetParent(buyMenuGrid.transform);
                temp.transform.localScale = Vector3.one;
                temp.GetComponent<InventoryPetal>().type = InventoryPetal.PetalType.MarketmenuBuy;
                temp.GetComponent<InventoryPetal>().upgradeType = i;
                temp.GetComponent<InventoryPetal>().UpdateSprite();
                buyItemList.Add(temp.GetComponent<InventoryPetal>());
            }
            else if(!tempUpgrades[7] && i == 7) // bow neck
            {
                GameObject temp = Instantiate(squarePrefab.gameObject);
                temp.transform.SetParent(buyMenuGrid.transform);
                temp.transform.localScale = Vector3.one;
                temp.GetComponent<InventoryPetal>().type = InventoryPetal.PetalType.MarketmenuBuy;
                temp.GetComponent<InventoryPetal>().upgradeType = i;
                temp.GetComponent<InventoryPetal>().UpdateSprite();
                buyItemList.Add(temp.GetComponent<InventoryPetal>());
            }
        }
    }

    /// <summary>
    /// Empties the current item list. Then starts filling the menu with the player's current inventory.
    /// It will only show the items that the player is able to sell. There will be a number listed under the icon to show
    /// how many of that item the player currently has.
    /// </summary>
    public void SellMenu()
    {
        Seed.Type tempSeed = Seed.Type.None;
        Sellables.Type tempSellableType = Sellables.Type.None;

        foreach (InventoryPetal item in sellItemList)
        {
            if (item == currentPetal)
            {
                currentPetal = null;
                if (item.seed != Seed.Type.None)
                {
                    tempSeed = item.seed;
                }
                else if (item.sellableType != Sellables.Type.None)
                {
                    tempSellableType = item.sellableType;
                }
            }

            Destroy(item.gameObject);
        }
        sellItemList.Clear();

        int nonValidItem = 0;
        for (int i = 0; i < inventory.inventorySeed.Count + nonValidItem; i++) //Can currently only sell seeds
        {
            GameObject temp = Instantiate(squarePrefab.gameObject);
            temp.transform.SetParent(sellMenuGrid.transform);
            temp.transform.localScale = Vector3.one;

            temp.GetComponent<InventoryPetal>().type = InventoryPetal.PetalType.MarketmenuSell;
            temp.GetComponent<InventoryPetal>().seed = Seed.Type.Normal + i;

            int t;

            if (inventory.inventorySeed.TryGetValue(temp.GetComponent<InventoryPetal>().seed, out t))
            {
                temp.GetComponent<InventoryPetal>().objectAmount = t;
                temp.GetComponent<InventoryPetal>().UpdateSprite();
                sellItemList.Add(temp.GetComponent<InventoryPetal>());

                if(temp.GetComponent<InventoryPetal>().seed == tempSeed && tempSeed != Seed.Type.None)
                {
                    currentPetal = temp.GetComponent<InventoryPetal>();
                }
            }
            else
            {
                Destroy(temp);
                nonValidItem++;
            }
        }

        nonValidItem = 0;

        for (int i = 0; i < inventory.inventorySellables.Count + nonValidItem; i++)
        {
            GameObject temp = Instantiate(squarePrefab.gameObject);
            temp.transform.SetParent(sellMenuGrid.transform);
            temp.transform.localScale = Vector3.one;

            temp.GetComponent<InventoryPetal>().type = InventoryPetal.PetalType.MarketmenuSell;
            temp.GetComponent<InventoryPetal>().sellableType = Sellables.Type.Fruit + i;

            int t;

            if (inventory.inventorySellables.TryGetValue(temp.GetComponent<InventoryPetal>().sellableType, out t))
            {
                temp.GetComponent<InventoryPetal>().objectAmount = t;
                temp.GetComponent<InventoryPetal>().UpdateSprite();
                sellItemList.Add(temp.GetComponent<InventoryPetal>());

                if (temp.GetComponent<InventoryPetal>().sellableType == tempSellableType && tempSellableType != Sellables.Type.None)
                {
                    currentPetal = temp.GetComponent<InventoryPetal>();
                }
            }
            else
            {
                Destroy(temp);
                nonValidItem++;
            }
        }
    }

    /// <summary>
    /// When the marketplace is enabled, the camera freezes and the mouse is visible on screen
    /// </summary>
    void OnEnable()
    {
        Cursor.visible = true;
        SellMenu();
        BuyMenu();
        mainManager.overlayOpen = true;
        mainManager.FreezeCam();
        Time.timeScale = 0;
    }

    /// <summary>
    /// When the market is disabled, the camera unfreezes
    /// </summary>
    void OnDisable()
    {
        Cursor.visible = false;
        mainManager.overlayOpen = false;
        mainManager.UnfreezeCam();
        Time.timeScale = 1;
    }

    /// <summary>
    /// This function is called from the inventoryPetal script
    /// Depending on the type of petal, it adds or removes seeds and objects to the inventory and adds or removes money from the inventory if able
    /// </summary>
    public void ItemBoughtSold()
    {
        if (currentPetal != null && currentPetal.type == InventoryPetal.PetalType.MarketmenuBuy)
        {
            if (inventory.renorks >= currentPetal.value)
            {
                if(currentPetal.seed != Seed.Type.None)
                {
                    inventory.AddToInventory(currentPetal.seed, 1);
                }
                else if (currentPetal.sellableType != Sellables.Type.None)
                {
                    inventory.AddToInventory(currentPetal.sellableType, 1);
                }
                else if(currentPetal.upgradeType != -1)
                {
                    inventory.GetComponent<PlayerUpgrades>().allUpgrade[currentPetal.upgradeType] = true;
                    if(currentPetal.upgradeType < 3)
                    {
                        inventory.GetComponent<PlayerUpgrades>().AddLevel(PlayerUpgrades.Type.Harvest);
                    }
                    else if (currentPetal.upgradeType < 6)
                    {
                        inventory.GetComponent<PlayerUpgrades>().AddLevel(PlayerUpgrades.Type.Water);
                    }
                    else if (currentPetal.upgradeType >= 6)
                    {
                        inventory.GetComponent<PlayerUpgrades>().AddLevel(PlayerUpgrades.Type.Bowtie);
                    }
                    BuyMenu();
                }
                inventory.renorks -= currentPetal.value;
                currentPetal.UpdateSprite();
            }
        }
        else if (currentPetal != null && currentPetal.type == InventoryPetal.PetalType.MarketmenuSell)
        {
            if(currentPetal.seed != Seed.Type.None)
            {
                inventory.RemoveFromInventory(currentPetal.seed, 1);
            }
            else if (currentPetal.sellableType != Sellables.Type.None)
            {
                inventory.RemoveFromInventory(currentPetal.sellableType, 1);
            }
            inventory.renorks += currentPetal.value;
            currentPetal.UpdateSprite();
        }
        
        SellMenu();
    }

    /// <summary>
    /// Updates the center of the screen based on which plant the player clicked on
    /// This includes the title, the information text and the value, as well as the text on the button (Buy, sell)
    /// </summary>
    /// <param name="item"></param>
    public void UpdateInformationCenter(InventoryPetal item)
    {
        if (item.type == InventoryPetal.PetalType.MarketmenuBuy)
        {
            buySellButton.GetComponentsInChildren<Image>()[1].sprite = buyText; //Cannot use getComponentInChildren, since it also searches parents and picks up parent image
        }
        else if (item.type == InventoryPetal.PetalType.MarketmenuSell)
        {
            buySellButton.GetComponentsInChildren<Image>()[1].sprite = sellText;
        }

        objectTitle.text = item.title;

        if (item.seed != Seed.Type.None)
        {
            switch (item.seed)
            {
                case Seed.Type.Normal:
                    objectDescription.text = "These plants grow rapidly, and it's easy to control and maintain their growth. It needs little water to survive, and seems to be affected strangely when hit by a solarflare. \n \n Value: " + item.value;
                    break;

                case Seed.Type.Shield:
                    objectDescription.text = "These plants have a defense mechanism that protects them from the solarflares. They emit a purple aura showing the protected area around them. Low water demand. \n \n Value: " + item.value;
                    break;

                case Seed.Type.Water:
                    objectDescription.text = "This large plant is filled with water. It has the ability to provide the soil and plants surrounding it with water, as well as itself. However, the soil around it will only get watered once it is fully grown. \n \n Value: " + item.value;
                    break;

                case Seed.Type.Light:
                    objectDescription.text = "This plant seems to be making the plants around it grow more rapidly. It needs a large amount of water to survive, and is usually not planted alone . It is often planted for cosmetic reasons as well. \n \n Value: " + item.value;
                    break;

                case Seed.Type.Fruit:
                    objectDescription.text = "This tree grows slowly, and is the only plant recorded to produce fruit. The fruit seems to be worth a lot on the market, as the plant is quite rare. \n \n Value: " + item.value;
                    break;

                case Seed.Type.Crocus:
                    objectDescription.text = "This is the seed of a very rare plant. It is not known to mutate from other plants, nor multiply by farming. The seed and head of this plant is worth a lot of money. \n \n Value: " + item.value;
                    break;
                default:
                    break;
            }
        }
        else if (item.sellableType != Sellables.Type.None)
        {
            switch (item.sellableType)
            {
                case Sellables.Type.Fruit:
                    objectDescription.text = "This fruit is harvested from the large fruit trees. It does not seem to have any special qualities, but it is very rare and valuable. \n \n Value: " + item.value;
                    break;
                case Sellables.Type.CommonHead:
                    objectDescription.text = "The head of the common plant you started your adventure with. It is not worth much, but it can be profitable in large quantities. \n \n Value: " + item.value;
                    break;
                case Sellables.Type.ShieldHead:
                    objectDescription.text = "The head of the shield plant. This large, pink flower might be beneficial for engineers to make shields. It is not very rare, however. \n \n Value: " + item.value;
                    break;
                case Sellables.Type.WaterHead:
                    objectDescription.text = "The head of the water plant. These are usually used to store water after they are harvested. This is not helpful to you, however. \n \n Value: " + item.value;
                    break;
                case Sellables.Type.LightHead:
                    objectDescription.text = "The head of the light plant. It still emits a dim, yellow light. After being harvested, these are usually used for decorative purposes. \n \n Value: " + item.value;
                    break;
                case Sellables.Type.CrocusHead:
                    objectDescription.text = "The head of a crocus plant. This is a very rare plant, and is worth a lot of money. There are very few recorded sightings of this plant, and it is unknown what it is used for. \n \n Value: " + item.value;
                    break;
                default:
                    break;
            }
        }
        else if(item.upgradeType != -1)
        {
            switch (item.upgradeType)
            {
                case 0:
                    objectDescription.text = "Makes you harvest slightly faster. The first of 3 harvest upgrades. \n \n Value:" + item.value;
                    break;
                case 1:
                    objectDescription.text = "Makes you harvest even faster. The second of 3 harvest upgrades. \n \n Value:" + item.value;
                    break;
                case 2:
                    objectDescription.text = "Makes you harvest very fast. The last of 3 harvest upgrades. \n \n Value:" + item.value;
                    break;
                case 3:
                    objectDescription.text = "Makes your water meter refill slightly faster. The first of 3 watering upgrades. \n \n Value:" + item.value;
                    break;
                case 4:
                    objectDescription.text = "Makes your water meter refill even faster. The second of 3 watering upgrades. \n \n Value:" + item.value;
                    break;
                case 5:
                    objectDescription.text = "Makes your water meter very fast. The last of 3 watering upgrades. \n \n Value:" + item.value;
                    break;
                case 6:
                    objectDescription.text = "It's nice to dress up sometimes. Adds a bow to your head \n \n Value: " + item.value;
                    break;
                case 7:
                    objectDescription.text = "It's nice to dress up sometimes. Adds a bow to your neck \n \n Value: " + item.value;
                    break;
                default:
                    break;
            }
        }

        currentPetal = item;
    }
}
