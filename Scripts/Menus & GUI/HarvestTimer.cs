﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HarvestTimer : MonoBehaviour 
{
	private Image image;	//To set the fill amount
	private InteractObject io;	//Get seedtime and harvesttime
	private float harvestTime;	//how long the player have been harvesting a plant.

	void Start()
	{
		image = GetComponent<Image>();
		io = FindObjectOfType<InteractObject>();
	}

	void Update() 
	{
		if(Input.GetMouseButtonUp(0))
		{
			image.fillAmount = 0;
			return;
		}
		
		if(io.GetCurrentObject() == null)
			return;
			
		if(io.GetHarvestTimer() == 0)
		{
			harvestTime = io.GetHarvestTime(io.GetCurrentObject().GetComponent<Seed>().seedType);	//Get the seed of the object the player want to harvest.
		}
		 image.fillAmount = io.GetHarvestTimer() / harvestTime;
	}
 
}
