﻿using System.Collections.Generic;
using UnityEngine;

public class FullInventory : MonoBehaviour
{
    private PlayerInventory inventory; //Reference to the inventory to display it correctly
    private InventoryPetal activeSquare; //Holds the square that the player is currently focuses on
    private MainManager mainManager; //Reference to the main manager
    public GameObject squarePrefab; //The base inventory square that gets instantiated and modified.
    public GameObject gridParent; //The parent of the object. This parent will but the objects in a grid layout
    public InventoryBarMenu barMenu; //Able to change the bar menu from the full inventory to customize it
    List<InventoryPetal> itemsInInventory = new List<InventoryPetal>(); //List of all items currently shown in the inventory
    
    void Awake()
    {
        inventory = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerInventory>();
        mainManager = GameObject.FindGameObjectWithTag("Manager").GetComponent<MainManager>();
    }

    void Update()
    {
        if (activeSquare != null) //Rebinds a petal to a slot on the bar if the player presses a number while having an active slot
        {
            InventoryPetal[] barMenuItems = barMenu.GetComponentsInChildren<InventoryPetal>();
            int buttonPressed = -1;

            if (Input.GetKeyDown("3"))
            {
                buttonPressed = 2;
            }
            else if (Input.GetKeyDown("4"))
            {
                buttonPressed = 3;
            }
            else if (Input.GetKeyDown("5"))
            {
                buttonPressed = 4;
            }
            else if (Input.GetKeyDown("6"))
            {
                buttonPressed = 5;
            }
            else if (Input.GetKeyDown("7"))
            {
                buttonPressed = 6;
            }
            else if (Input.GetKeyDown("8"))
            {
                buttonPressed = 7;
            }
            else if (Input.GetKeyDown("9"))
            {
                buttonPressed = 8;
            }

            if (buttonPressed != -1)
            {
                //Rebinds the slot if a button is pressed
                barMenu.slots[buttonPressed].ClearPetal();
                barMenu.slots[buttonPressed].seed = activeSquare.seed;
                barMenu.slots[buttonPressed].sellableType = activeSquare.sellableType;
                barMenu.slots[buttonPressed].tool = activeSquare.tool;
                barMenu.slots[buttonPressed].objectAmount = activeSquare.objectAmount;
                barMenu.slots[buttonPressed].UpdateSprite();

                for (int i = 0; i < barMenuItems.Length; i++)
                {
                    if (i != buttonPressed)
                    {
                        if (barMenu.slots[buttonPressed].seed == barMenuItems[i].seed && barMenu.slots[buttonPressed].tool == barMenuItems[i].tool)
                        {
                            barMenuItems[i].ClearPetal();
                        }
                    }
                }
            }
        }
    }

    /// <summary>
    /// Empties the menu completely, then redoes it based on the player's inventory. Tools are always first
    /// </summary>
    void RedoMenu() 
    {
        GameObject temp = Instantiate(squarePrefab);
        temp.GetComponent<InventoryPetal>().tool = PlayerInventory.Tool.Watercan;
        temp.GetComponent<InventoryPetal>().UpdateSprite();
        temp.transform.SetParent(gridParent.transform);
        temp.transform.localScale = Vector3.one;
        itemsInInventory.Add(temp.GetComponent<InventoryPetal>());

        temp = Instantiate(squarePrefab);
        temp.GetComponent<InventoryPetal>().tool = PlayerInventory.Tool.Shovel;
        temp.GetComponent<InventoryPetal>().UpdateSprite();
        temp.transform.SetParent(gridParent.transform);
        temp.transform.localScale = Vector3.one;
        itemsInInventory.Add(temp.GetComponent<InventoryPetal>());

        int t;

        for (int i = 0; i < 6; i++)
        {
            if (inventory.inventorySeed.TryGetValue(Seed.Type.Normal + i, out t))
            {
                temp = Instantiate(squarePrefab);
                temp.GetComponent<InventoryPetal>().ClearPetal();
                temp.GetComponent<InventoryPetal>().seed = Seed.Type.Normal + i;
                temp.GetComponent<InventoryPetal>().objectAmount = t;
                temp.GetComponent<InventoryPetal>().UpdateSprite();
                temp.transform.SetParent(gridParent.transform);
                temp.transform.localScale = Vector3.one;
                itemsInInventory.Add(temp.GetComponent<InventoryPetal>());
            }
        }

        for (int i = 0; i < 7; i++)
        {
            if (inventory.inventorySellables.TryGetValue(Sellables.Type.Fruit + i, out t))
            {
                temp = Instantiate(squarePrefab);
                temp.GetComponent<InventoryPetal>().ClearPetal();
                temp.GetComponent<InventoryPetal>().sellableType = Sellables.Type.Fruit + i;
                temp.GetComponent<InventoryPetal>().objectAmount = t;
                temp.GetComponent<InventoryPetal>().UpdateSprite();
                temp.transform.SetParent(gridParent.transform);
                temp.transform.localScale = Vector3.one;
                itemsInInventory.Add(temp.GetComponent<InventoryPetal>());
            }
        }
    }

    public void SetActiveSquare(InventoryPetal square)
    {
        activeSquare = square;
    }

    void OnEnable()
    {
        RedoMenu();
        mainManager.overlayOpen = true;
        mainManager.FreezeCam(); //Freezes cam rotation to avoid feeling of sickness
        Cursor.visible = true;
    }

    void OnDisable()
    {
        activeSquare = null;
        mainManager.overlayOpen = false;
        mainManager.UnfreezeCam();
        Cursor.visible = false;

        foreach (InventoryPetal squareTemp in itemsInInventory)
        {
            Destroy(squareTemp.gameObject);
        }
        itemsInInventory.Clear();
    }
}
