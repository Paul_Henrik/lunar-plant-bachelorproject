﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    Clock timeManager; //Need to pause the time
    GameObject currentCanvas; //Need to know which canvas is currently active
    GameObject pauseMenu; //Holds the pause menu canvas
    GameObject settingsMenu; //Holds the settings menu canvas
    bool continueButton = false; //Need to know whether the continue button was pressed
    MainManager mainManager; //Access to general functions in mainManager

    /// <summary>
    /// Get all nesseccary values. Assign the pause menu canvas to the currentCanvas, since we always want to start on that canvas
    /// Also turn all canvases off.
    /// </summary>
    void Start()
    {
        mainManager = GameObject.FindGameObjectWithTag("Manager").GetComponent<MainManager>();
        timeManager =mainManager.GetComponent<Clock>();
        pauseMenu = GameObject.FindGameObjectWithTag("PauseMenu");
        settingsMenu = GameObject.FindGameObjectWithTag("SettingsMenu");
        pauseMenu.SetActive(false);
        settingsMenu.SetActive(false);
        currentCanvas = pauseMenu;
    }

    //If continue button is pressed, the canvas will close and time will be unpaused
    public void ContinueButton()
    {
        continueButton = true;
    }

    //Change which canvas is currently active
    public void Settings()
    {
        currentCanvas = settingsMenu;
    }

    //Return to the main menu scene
    public void ExitToMenuButton()
    {
        SceneManager.LoadScene(0);
    }

    //Close the application
    public void ExitToDesktopButton()
    {
        Application.Quit();
    }

    //If a back button is pressed, will return to the pause menu
    public void BackButton()
    {
        currentCanvas = pauseMenu;
    }

    /// <summary>
    /// Pauses time via the clock script
    /// Freezes all constraints for the player, so they cannot move while the game is paused
    /// If the game is unpaused, the player movement is freed again
    /// </summary>
    void PauseTime()
    {
        timeManager.pauseTime = !timeManager.pauseTime;
        timeManager.timeScale = (timeManager.timeScale == 1) ? 0 : 1;

        if (timeManager.pauseTime)
        {
            Cursor.visible = true;
            mainManager.overlayOpen = true;
            Cursor.lockState = CursorLockMode.Confined;
        }
        else
        {
            Cursor.visible = false;
            mainManager.overlayOpen = false;
            Cursor.lockState = CursorLockMode.Confined;
        }
    }

    /// <summary>
    /// If the escape button or continue button is pressed.
    /// If we are currently on another canvas than the pause menu, we return to the pause menu
    /// If we are at the pause menu, we'll run the pause time function, as well as making the canvas active the opposite of what it currently is.
    /// Sets continue button bool to false at the end.
    /// </summary>
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape) || continueButton)
        {
            if (currentCanvas != pauseMenu)
            {
                currentCanvas.SetActive(false);
                currentCanvas = pauseMenu;
                currentCanvas.SetActive(true);
            }
            else if (currentCanvas == pauseMenu)
            {
                PauseTime();
                currentCanvas.SetActive(timeManager.pauseTime);
            }

            continueButton = false;
        }
    }
}
